from multiprocessing import Process, Manager
from itertools import combinations, product
import re
import subprocess
from models.Planner import *
import os

class Jarvis(object):

    " Cérebro do processo"
    def __init__(self, arguments, agentes):

        self.args = arguments
        self.agentes = agentes
        self.initialState = set()
        self.goal = set()
        self.str_initialState = set()
        self.str_goal = set()

        self.manager = Manager()
        self.plans = self.manager.dict()

        self.dictAgentes = {}

        self.commonObjects = None

        #self.goalObjects = set()

        for ag in self.agentes:
            self.unionInitialState(ag.initialState)
            self.unionGoal(ag.goal)
            self.plans[ag.agId] = None
            self.dictAgentes[ag.agId] = ag
            if(self.commonObjects == None):
                self.commonObjects = ag.objects
            else:
                self.commonObjects = self.commonObjects.intersection(ag.objects)       

    def unionInitialState(self,pList):
        for p in pList:
            if(str(p) not in self.str_initialState):
                self.initialState.add(p)
                self.str_initialState.add(str(p))

    def unionGoal(self,pList):
        for p in pList:
            if(str(p) not in self.str_goal):
                self.goal.add(p)
                self.str_goal.add(str(p))

    def assignment(self,candidates,isMap):

        assignedGoals = set()
        currentAgent = 0
        responsibleAgent = {}

        if(not isMap):
            if(self.args.lb): # usar balanceamento de carga entre agentes
                # ordenar candidatos em ordem crescente de qtde de possibleGoals
                candidates = list(sorted(candidates,key=self.getPossibleGoalsLength))
            else:
                candidates = list(sorted(candidates,key=self.getPossibleGoalsLength, reverse=True))

            while(len(assignedGoals) < len(self.goal)):
                if(self.args.lb): # usar balanceamento de carga entre agentes
                    if(len(candidates[currentAgent].possibleGoals)>0):
                        goal = candidates[currentAgent].possibleGoals.pop()
                        if(candidates[currentAgent].agId not in responsibleAgent):
                            responsibleAgent[candidates[currentAgent].agId] = set()
                        responsibleAgent[candidates[currentAgent].agId].add(goal)
                        assignedGoals.add(goal)
                        for ag in candidates:
                            toBeRemoved = set()
                            for g in ag.possibleGoals:
                                if(str(g) == str(goal)):
                                    toBeRemoved.add(g)
                            ag.possibleGoals = ag.possibleGoals.difference(toBeRemoved)

                    currentAgent = currentAgent + 1
                    if(currentAgent>=len(candidates)):
                        currentAgent = 0

                else: #usar o menor número de agentes
                    justAssigned = set()
                    maximo = 0
                    candidates = sorted(candidates,key=self.getPossibleGoalsLength, reverse=True)
                    maxAgent=candidates[0]

                    for goal in maxAgent.possibleGoals:
                        if(maxAgent.agId not in responsibleAgent):
                            responsibleAgent[maxAgent.agId] = set()
                        responsibleAgent[maxAgent.agId].add(goal)
                        assignedGoals.add(goal)
                        justAssigned.add(str(goal))

                    candidates.remove(maxAgent)

                    for ag in candidates:
                        toBeRemoved = set()
                        for g in ag.possibleGoals:
                            if(str(g) in justAssigned):
                                toBeRemoved.add(g)
                        ag.possibleGoals = ag.possibleGoals.difference(toBeRemoved)

            return ("SAP",responsibleAgent)

        else: # tratar tudo como MAP, i.e, planejamento centralizado

            qtde = 2
            team = combinations(candidates, r = qtde)
            mapSolution = False
            selecetedTeam = None
            #while(not mapSolution and (qtde <=len(self.agentes))):
            while(qtde <=len(self.agentes)):
                for t in team:
                    if(self.HFF(self.initialState,self.goal,t)):
                        return ("MAP",t)
                qtde = qtde + 1
                team = combinations(candidates, r=qtde)

            return ("MAP", self.agentes)

            '''
            if(mapSolution):
                for ag in selecetedTeam:
                    responsibleAgent[ag.agId] = set()
            else:
                print("Um dos objetivos é impossível")
                end = time.time()
                print('ASS = ',end - start)
                return ("MAP", self.agentes)

            return ("MAP",selecetedTeam)
            '''
            
    def selection(self,isMap):

        if(isMap):
            return list(self.agentes)
        else:
            # seleciona apenas os agentes que podem resolver pelo menos um subgoal
            candidates = list([c for c in self.agentes if len(c.possibleGoals)>0])
            return candidates

    def coordinate(self):

        if(len(self.agentes)>1):

            individualPossibleGoals = set()
            for ag in self.agentes:
                for g in ag.possibleGoals:
                    individualPossibleGoals.add(str(g))

            isMap = (len(individualPossibleGoals) != len(self.goal))

            candidates = self.selection(isMap)
            subProblems = self.assignment(candidates,isMap)
            planValidation = None
            planTests = None
            objConcurrency = False

            if(subProblems[0] == "SAP"):
                self.individualPlanning(subProblems[1])

                planValidation,commonTokens = self.validatePlan(subProblems[1].keys())

                if(not planValidation):
                    print('Planos encontrados não são mutualmente válidos')

                planTests = self.planTest(subProblems[1])

                objConcurrency = self.checkCommonTokens(commonTokens)

                if(planTests and planValidation): #todos acharam um plano e são validos juntos
                    
                    # verificar os planos são validos e se podem ser executados em paralelo
                    # Algoritmo do artigo de Petri Nets
                    nrAgentes = 0
                    nrAcoes = 0
                    for ag in subProblems[1].keys():
                        print('Agente',ag)
                        print('\tGoals:',subProblems[1][ag])
                        print('\tPlano:')
                        if(self.plans[ag] != None):
                            self.printPlan(self.plans[ag])
                            nrAgentes = nrAgentes + 1
                            nrAcoes = nrAcoes + len(self.plans[ag])

                    print('Estatisticas finais:')
                    print('Total de agentes:',nrAgentes)
                    print('Total de acoes:',nrAcoes)

                elif(objConcurrency):
                    print("Sem solução por existir concorrência")            
            
            if(not objConcurrency) and ((subProblems[0] == "MAP") or (not planTests) or (not planValidation)):
                qtde = len(subProblems[1])
                #qtde = 2
                team = combinations(subProblems[1], r = qtde)
                mapSolution = False
                selecetedTeam = None
                if(qtde>0):#existe algum arranjo de agentes capaz de resolver
                    while(not mapSolution and (qtde <=len(self.agentes))):
                        for t in team:
                            if(not mapSolution):
                                plano = self.centralizedPlan_Super(self.initialState, self.goal,t)
                                if(plano != None):
                                    mapSolution = True
                                    print('\tPlano multi-agente:')
                                    print('\tAgentes empregados:',qtde)
                                    self.printPlan(plano)
                                    break
                                #break
                        if(mapSolution):
                            qtde = len(self.agentes) + 1
                            break
                        else:
                            qtde = qtde + 1
                            team = combinations(candidates, r=qtde)

                if(not mapSolution): #não achou nenhum plano
                    print("Sem solução")
        else:
            ag = self.agentes[0]
            plano = ag.plan(ag.initialState,ag.goal)
            print('Agente',ag.agId)
            print('\tGoals:',ag.goal)
            print('\tPlano:')
            if(plano != None):
                self.printPlan(plano)
            else:
                 print('\t\tSem solução')

    def individualPlanning(self, subProblems):

        if(self.args.p):
            process = [None]*len(subProblems)
            i = 0
            for ag in subProblems.keys():
                process[i]= Process(target = self.getAgentPlan, args = (subProblems[ag],self.plans,ag))
                process[i].start()
                i = i + 1
            
            i = 0
            for i in range(0,len(subProblems)):
                process[i].join()
                i = i + 1
        else:
            print('planning serial')
            for ag in subProblems.keys():
                i = 0
                c = 0
                for a in self.agentes:
                    if(a.agId == ag):
                        i = c
                    else:
                        c = c + 1
                self.plans[ag] = self.agentes[i].getPlan(subProblems[ag])

    def planTest(self,subProblems):

        teste = []
        # checando se todos conseguiram 
        for ag in subProblems.keys():
            teste.append(self.plans[ag]!=None)
        return (all(teste))


    def getAgentPlan(self,goal,plan,ag):

        i = 0
        c = 0
        for a in self.agentes:
            if(a.agId == ag):
                i = c
            else:
                c = c + 1
        plan[ag] = self.agentes[i].getPlan(goal)
        print('Agente',ag, 'calculou um plano!')

    def centralizedPlan_Super(self,initialState, goal,agentes):

        dictObjects = {}
        for ag in agentes:
            for obj in ag.objects:
                if obj[1] not in dictObjects:
                    dictObjects[obj[1]] = set()
                dictObjects[obj[1]].add(obj[0])

        setActions = set()
        for ag in agentes:
            for a in ag.actions:
                setActions.add(a)


        #############################Domain File ######################################
        #creating problem file 
        file = open("tmpDomain.pddl","w")

        #opening
        file.write('(define\n')
        file.write('\t(domain tmpDomain)\n')
        file.write('\t(:requirements :typing)\n')
        file.write('\t(:types')
        
        #types
        for tipo in dictObjects.keys():
            file.write(' '+tipo)
        file.write(')\n')

        #predicates

        file.write('\t(:predicates')

        setPredicates = set()
        setPredicateNames = set()
        dictParameters = {}
        dictPrePos = {}
        dictPreNeg = {}
        dictEffPos = {}
        dictEffNeg = {}
        for ag in agentes:
            for action in ag.actions:
                parameters = ag.parser.getActionParameters(action)
                if(action not in dictParameters):
                    dictParameters[action] = parameters
                #verificando se alguma acao tem algum parametro sem algum objeto do mesmo tipo
                # se tiver...crio um virtual
                for parameterType in parameters.values():
                    if parameterType not in dictObjects:
                        dictObjects[parameterType] = 'virtual'+parameterType
                dictPrePos[action] = ag.positivePreconditionsByAction[action]
                for p in ag.positivePreconditionsByAction[action]:
                    if(p.predicate[0] not in setPredicateNames):
                        strPredicate = '('+p.predicate[0]
                        for var in p.predicate[1:]:
                            strPredicate+=' '+var+' - ' +parameters[var]
                        strPredicate+=')'
                        setPredicates.add(strPredicate)
                        setPredicateNames.add(p.predicate[0])

                dictPreNeg[action] = ag.negativePreconditionsByAction[action]
                for p in ag.negativePreconditionsByAction[action]:
                    if(p.predicate[0] not in setPredicateNames):
                        strPredicate = '('+p.predicate[0]
                        for var in p.predicate[1:]:
                            strPredicate+=' '+var+' - ' +parameters[var]
                        strPredicate+=')'
                        setPredicates.add(strPredicate)
                        setPredicateNames.add(p.predicate[0])

                dictEffPos[action] = ag.positiveEffectsByAction[action]

                for p in ag.positiveEffectsByAction[action]:
                    if(p.predicate[0] not in setPredicateNames):
                        strPredicate = '('+p.predicate[0]
                        for var in p.predicate[1:]:
                            strPredicate+=' '+var+' - ' +parameters[var]
                        strPredicate+=')'
                        setPredicates.add(strPredicate)
                        setPredicateNames.add(p.predicate[0])

                dictEffNeg[action] = ag.negativeEffectsByAction[action]

                for p in ag.negativeEffectsByAction[action]:
                    if(p.predicate[0] not in setPredicateNames):
                        strPredicate = '('+p.predicate[0]
                        for var in p.predicate[1:]:
                            strPredicate+=' '+var+' - ' +parameters[var]
                        strPredicate+=')'
                        setPredicates.add(strPredicate)
                        setPredicateNames.add(p.predicate[0])

        for p in setPredicates:
            file.write('\n\t\t'+p)


        file.write('\n\t)')

        #actions
        for action in setActions:
            file.write('\n\t(:action '+str(action)+'\n')
            #parameters
            file.write('\t\t:parameters (')
            pars1 =re.sub('[\',\{\}]','',str(dictParameters[action]))
            pars2 =re.sub(':',' -',pars1)
            file.write(pars2+')')
            
            #preconditions
            file.write('\n\t\t:precondition\n')
            file.write('\t\t\t(and\n')
            if(len(dictPrePos[action])>0):
                prePos =re.sub('[\',\{\}]','',str(dictPrePos[action]))
                file.write('\t\t\t\t'+prePos)

            if(len(dictPreNeg[action])>0):
                preNeg1 =re.sub('[\',\{\}]','',str(dictPreNeg[action]))
                preNeg2 =re.sub('\(','(not(',preNeg1)
                preNeg3 =re.sub('\)','))',preNeg2)
                file.write('\n\t\t\t\t'+preNeg3)

            file.write('\n\t\t\t)\n')

            #effects
            file.write('\n\t\t:effect\n')
            file.write('\t\t\t(and\n')

            if(len(dictEffPos[action])>0):
                effPos =re.sub('[\',\{\}]','',str(dictEffPos[action]))
                file.write('\t\t\t\t'+effPos)

            if(len(dictEffNeg[action])>0):
                effNeg1 =re.sub('[\',\{\}]','',str(dictEffNeg[action]))
                effNeg2 =re.sub('\(','(not(',effNeg1)
                effNeg3 =re.sub('\)','))',effNeg2)
                file.write('\n\t\t\t\t'+effNeg3)

            file.write('\n\t\t\t)')
            file.write('\n\t)\n')

        file.write(')\n')
        file.close()

        
        #############################Problem File ######################################
        #creating problem file 
        file = open("tmpProblem.pddl","w")

        #opening
        file.write('(define\n')
        file.write('\t(problem tmpProblem)\n')
        file.write('\t(:domain tmpDomain)\n')
        file.write('\t(:objects\n')

        for tipo, obj in dictObjects.items():
            o = re.sub('[\',\{\}]','',str(obj))
            file.write('\t\t'+o+' - '+str(tipo)+'\n')

        file.write('\t)\n')


        #initialState

        file.write('\t(:init\n')
        agentsInitialState = set()
        strAgentsInitialState = set()
        for ag in agentes:
            for p in ag.initialState:
                if(str(p) not in strAgentsInitialState):
                    agentsInitialState.add(p)
                    strAgentsInitialState.add(str(p))
        
        #for initState in self.initialState:
        for initState in agentsInitialState:
            i = re.sub('[\',]','',str(initState))
            file.write('\t\t'+i+'\n')

        file.write('\t)\n')

        #goals
        file.write('\t(:goal\n\t\t(and\n')

        for g in goal:
            t = re.sub('[\',]','',str(g))
            file.write('\t\t\t'+t+'\n')

        file.write('\t\t)\n\t)\n')

        #closing
        file.write(')')
        file.close()
        #############################Problem File ######################################

        planner = Planner('./utils/FF-v2.3/','ff -o %s -f %s')
        plano = planner.toGops(planner.callPlanner('tmpDomain.pddl','tmpProblem.pddl'),agentes,dictParameters)
        os.remove('tmpDomain.pddl')
        os.remove('tmpProblem.pddl')
        return plano

    def getPossibleGoalsLength(self,ag):

        return(len(ag.possibleGoals))

   
    def HFF(self,state, goal,agentes):

        """
        Calcula a heurística HFF do Ghalab ag 53.
        Na verdade estou usando pra verificar se o goal é possível
        """

        hatS = []
        hatS.append(state)      

        rApplicableActions = [] 
        rApplicableActions.append(None)
        k=1
        #print(agentes)
        while(not agentes[0].satisfies(hatS[k-1],goal)):
            #pode usar qualquer agente
            currentStatePredicates = agentes[0].parser.getPredicatesInState(hatS[k-1])
            
            newState = hatS[k-1].copy()
            setStrState = set()
            for s in hatS[k-1]:
                setStrState.add(str(s))

            for ag in agentes:
                for action in ag.actions:
                    if(ag.requiredPredicatesByAction[action].issubset(currentStatePredicates)):

                        parametros = ag.configurations[action][0]
                        indiceParametro = ag.configurations[action][1]

                        for configuracao in product(*parametros):
                            #rotulo = ag.agId + '='+action + str(configuracao)
                            rotulo = action + str(configuracao)
                            
                            if (rotulo not in ag.gops):
                                ag.gops[rotulo] = ag.groundAction(action,configuracao,indiceParametro)

                            if(ag.gops[rotulo].precondition_pos.issubset(setStrState) and len(ag.gops[rotulo].precondition_neg.intersection(setStrState)) == 0):
                                newState = newState.union(ag.gops[rotulo].effect_pos)

            hatS.append(newState)

            setK0 = set()
            setK1 = set()

            for p in hatS[k]:
                setK0.add(str(p))

            for p in hatS[k-1]:
                setK1.add(str(p))

            if(setK0 == setK1):
                return False
            else:
                k = k +1
        return True        

    def printPlan(self, plan):

        if (plan == []):
            print('\t\tObjetivo já satisfeito. Nenhuma ação necessária!')
        else:
            for gop in plan:
                print('\t\t',gop.operator_name, gop.variable_list)
        print('\tTotal de ações:',len(plan))

    def validatePlan(self, agentes):

        precond = set()
        effect = set()

        tokens = {}

        for ag in agentes:
            plan = self.plans[ag]
            precond = set()
            effect = set()

            #print('testando ',ag, plan)
            
            if(plan != None):
                for gop in plan:
                    #precond = precond.union(gop.precondition_pos)

                    for p in gop.precondition_pos:
                        fimDoPredicado = p[2:-1].index('\'') + 2
                        tmp = p[2:fimDoPredicado]
                        if tmp not in self.dictAgentes[ag].rigidPredicates:
                            precond.add(p)

                    for p in gop.precondition_neg:
                        fimDoPredicado = p[2:-1].index('\'') + 2
                        tmp = p[2:fimDoPredicado]
                        if tmp not in self.dictAgentes[ag].rigidPredicates:
                            precond.add(p)

                    for e in gop.effect_pos:
                        fimDoPredicado = str(e)[2:-1].index('\'') + 2
                        tmp = str(e)[2:fimDoPredicado]
                        if tmp not in self.dictAgentes[ag].rigidPredicates:
                            effect.add(str(e))

                    for e in gop.effect_neg:
                        fimDoPredicado = str(e)[2:-1].index('\'') + 2
                        tmp = str(e)[2:fimDoPredicado]
                        if tmp not in self.dictAgentes[ag].rigidPredicates:
                            effect.add(str(e))

                tokens[ag] = precond.union(effect)
        
        duplas = combinations(agentes, r = 2) # todas combinacoes de dois agentes
        comparacoes = []
        commonTokens = set()
        for d in duplas:
            if len(tokens[d[0]].intersection(tokens[d[1]])) > 0:
                print(d[0],' e ',d[1],"\n\tPredicados conflitantes = ", tokens[d[0]].intersection(tokens[d[1]]))
            commonTokens.add(str(tokens[d[0]].intersection(tokens[d[1]])))
            comparacoes.append(len(tokens[d[0]].intersection(tokens[d[1]]))==0)
        
        return all(comparacoes),commonTokens

    def checkCommonTokens(self,tokens):

        tests = []
        for token in tokens:
            t = re.sub('[\{\}\(\)\"\"\s\'\']','',token)
            for p in t.split(','):
                tests.append(p in self.commonObjects)

        return any(tests)
