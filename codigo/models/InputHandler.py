from models.Agent import *
import json
from multiprocessing import Process, Manager

class InputHandler(object):
    """
    Responsável por transformar as informações passadas em arquivos JSON para um lista do tipo Agentes.
    A razão da utilização de arquivos JSON para definir as entradas foi tentar isolar do código as configurações 
    usadas nos testes. 
    Dessa forma, também será possível fazer um mapeamento entradas -> resultados o que é mais fácil de reproduzir
    """

    def __init__(self,args):

        with open(args.inputFile)as input_file:
            data = json.load(input_file)

        
        manager = Manager()
        self.agentes = manager.list()
        print("Instanciando agentes ...")
        
        if(args.p) and len(data["agentes"])>1: #execucao paralela
            process = [None]*len(data["agentes"])
            for i in range(0,len(data["agentes"])):
                process[i]= Process(target = self.createAgent, args = (data["agentes"][i],self.agentes))
                process[i].start()
            
            for i in range(0,len(data["agentes"])):
                process[i].join()

        else:
            for ag in data["agentes"]:
                self.agentes.append(Agent(ag["agId"],ag["domainPath"],ag["problemPath"]))

        print("Agentes instanciados!")
        
    def getInputs(self):

        return list(self.agentes)

    def createAgent(self, ag, listaAgentes):

        listaAgentes.append(Agent(ag["agId"],ag["domainPath"],ag["problemPath"]))
