import argparse
from models.InputHandler import *
from models.Jarvis import *
import time

if __name__ == '__main__':

    startF = time.time()
    parser = argparse.ArgumentParser(description='MAP3 - Multi Agent Planner with Pre-planning')
    parser.add_argument('inputFile')
    parser.add_argument('-p',action='store_true',help='execucao em paralelo')
    parser.add_argument('-lb',action='store_true',help='estrategia de delegacao: balanceamento de carga')
    args = parser.parse_args()
    inputHandler = InputHandler(args)
    jarvis = Jarvis(args,inputHandler.getInputs())
    #jarvis.coordinatePlan()
    #print(jarvis.assignment().keys())
    jarvis.coordinate()
    endF = time.time()
    print("Terminado com ", endF - startF, "seconds")
    '''
    print("\tInput ", end1 - start1, "seconds")
    print("\tjarvis ", end2 - end1, "seconds")
    print("\tcoordinate ", endF - end2, "seconds")
    '''

