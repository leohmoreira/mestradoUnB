Nesse capítulo é apresentada uma breve introdução sobre revisão sistemática e como ela foi aplicada na dissertação no contexto da 
revisão da literatura.

\section{Introdução}
\label{cap2:intro}

Após a definição do problema a ser investigado em uma pesquisa, é comum entre os pesquisadores e também importante que a próxima 
atividade seja a revisão da literatura. Entretanto, se essa revisão não for realizada de maneira completa e metódica, o seu valor 
científico é baixo. Sendo assim, uma maneira de agregar valor e credibilidade aos resultados é através da realização de 
uma revisão sistemática ao invés de simplesmente uma revisão da literatura.

Segundo \citet{kitchenham2004}, uma revisão sistemática da literatura é um meio de identificar, avaliar e interpretar todas as 
pesquisas disponíveis relevantes para uma determinada questão de pesquisa, área de tópico ou fenômeno de interesse. Entre os 
motivos que justificam a importância de sua realização, destacam-se a identificação de trabalhos correlatos, a investigação de 
lacunas e oportunidades de melhorias e o posicionamento adequado de novas atividades na área de pesquisa. 

Em relação a suas principais características, pode-se destacar que uma revisão sistemática:
\begin{itemize}
 \item começa pela definição de um protocolo que especifica a questão de pesquisa e os métodos que serão utilizados;
 \item baseia-se em uma estratégia de busca para encontrar o maior número de trabalhos relacionados possível;
 \item registra essa estratégia de forma a permitir sua reprodução por outros pesquisadores;
 \item exige critérios claros para inclusão e exclusão do trabalhos identificados;
 \item especifica quais informações serão buscadas de cada trabalho e como serão qualificadas;
 \item é executada em três fases: planejamento, execução e publicação.
\end{itemize}

Na fase de planejamento é verificada a existência de alguma revisão sistemática relacionado à área de pesquisa e com critérios 
semelhantes de avaliação. Dessa forma, se houver trabalhos relacionados já publicados, estes deverão ser utilizados. Por outro 
lado, na ausência deles, um protocolo de revisão deve ser elaborado para permitir a realização da revisão de maneira imparcial, 
sem vícios e livre de direcionamentos frutos de expectativas individuais dos pesquisadores. De maneira geral, nesse protocolo são 
definidos a questão de pesquisa, a estratégia de busca incluindo os termos e fontes de buscas, os critérios de seleção dos 
trabalhos, a avaliação de qualidade dos trabalhos incluídos, o conjunto de informações a ser obtido de cada item selecionado, a 
síntese dos dados obtidos e um cronograma de execução.

A questão de pesquisa destaca os principais tópicos a serem investigados e sua estrutura considera os seguinte pontos:
\begin{itemize}
 \item População: quem ou que será foco da pesquisa;
 \item Intervenção: aspectos específicos a serem pesquisados;
 \item Resultados: o que se deseja atingir;
 \item Contexto: quais são as circunstâncias.
\end{itemize}
 
Na fase de execução e com o protocolo já definido, a revisão sistemática é realizada em cinco etapas. Primeiramente, a estratégia 
de busca é definida pela escolha dos termos (palavras-chaves) e fontes de pesquisa (periódicos, conferências etc). De posse dos 
trabalhos obtidos, na segunda fase é realizada a seleção dos estudos seguindo os critérios de inclusão e exclusão, identificando 
aqueles potencialmente relevantes à pesquisa. Em seguida, os itens escolhidos são qualificados seguindo um critério definido pelo 
pesquisador a fim de organizá-los de acordo com importância e relevância. Nessa terceira fase de avaliação de qualidade, o 
critério a ser seguido é composto, preferencialmente, por um conjunto de perguntas respondidas por sim ou não. Consequentemente, 
um número maior de respostas afirmativas implica em um grau maior de qualidade. Na quarta etapa, dados específicos são obtidos 
de cada estudo buscando responder as questões de pesquisa. Por fim, os dados são sumarizados em forma de gráficos ou tabelas.

Na fase de publicação, os resultados da revisão sistemática podem ser compartilhados tanto na forma de um trabalho apresentado em 
periódicos e conferências quanto como parte de uma dissertação ou tese.

A fim de resumir a ordem cronológica das tarefas necessárias para a executadas de uma revisão sistemática, a 
Tabela \ref{cap2:cronograma} apresenta as atividades que foram realizadas na aplicação da técnica nessa dissertação.

\begin{table}[!htp]
 \centering
 \caption{Fases e atividades de uma revisão sistemática.}
 \begin{tabular}{|c|c|}
  \hline
  Fase & Atividades \\ \hline
  \multirow{3}{*}{Planejamento} & Identificar a necessidade de revisão \\ \cline{2-2}
& Especificar questão de pesquisa \\ \cline{2-2}
& Desenvolver o protocolo da revisão \\ \hline
  \multirow{5}{*}{Execução}     & Identificar pesquisa \\ \cline{2-2}
& Selecionar dos estudos primários \\ \cline{2-2}
& Avaliar da qualidade dos estudos \\ \cline{2-2}
& Extrair e monitorar os dados \\ \cline{2-2}
& Sintetizar os dados \\ \hline
  \multirow{3}{*}{Publicação}   & Especificar mecanismo de disseminação \\ \cline{2-2}
& Formatar o relatório principal \\ \cline{2-2} 
& Validar o relatório \\ \hline
 \end{tabular}
 \label{cap2:cronograma}
\end{table}

\section{Aplicação}

Seguindo o roteiro sugerido por \citet{kitchenham2004} e apresentado na Tabela \ref{cap2:cronograma}, a primeira atividade 
realizada foi a identificação da necessidade de uma revisão na área de pesquisa de planejamento multiagente. Nesse 
sentido, foram realizadas buscas em veículos de publicação de trabalhos científicos como \textit{ACM Digital Library, IEEExplore 
Digital Library, Springer, Google Scholar}. Devido à ausência de um trabalho correlato, foram iniciadas as atividades de 
definição da questão de pesquisa e do protocolo a ser seguido. Cabe ressaltar que visando abranger a maior quantidade possível de 
trabalhos, os termos utilizados nas buscas foram definidos na língua inglesa uma vez que grande parte dos trabalhos são 
publicados nesse idioma.

% \subsection{Questão de pesquisa}

A questão central da revisão sistemática bem como sua estrutura foram definidas conforme apresentado a seguir:

\begin{itemize}
 \item Questão de pequisa: Quais são os efeitos da execução de um pré-planejamento para um planejamento multiagente?
 \item População: \textit{Multi-agent Systems, Automated Planning, Multi-agent Planning};
 \item Intervenção: modelos, algoritmos e abordagens que tratam de seleção de agentes, validação e verificação em planejamento 
multiagente;
 \item Resultados: volume menor de mensagens e respostas mais rápidas em comparação com trabalhos que configuram o estado da arte;
 \item Contexto: \textit{Multi-agent Systems, Automated Planning, Multi-agent Planning}.
\end{itemize}

Com a questão já definida, a definição do protocolo de pesquisa foi iniciada pela estratégia de busca. Os termos, as fontes e os 
filtros utilizados nas pesquisas são apresentados na Tabela \ref{cap2:estratégia}.

A condição para inclusão dos resultados das pequisas foi definida por um único critério. Para ser considerado relevante, o 
trabalho deveria apresentar modelos ou algoritmos aplicáveis em planejamento multiagente. Por outro lado, os trabalhos que 
não focassem em planejamento multiagente ou que fossem dependentes de domínio foram excluídos. Essa condição foi necessária para 
ignorar trabalhos focados em cenários restritos como aplicação em tráfego aéreo, manutenção da privacidade das 
informações dos agentes, composição de \textit{web services} e planejamento de rotas (\textit{path planning}). Portanto, trabalhos 
com maior abrangência e independente de domínio foram 
priorizados.

\begin{table}[htp]
  \centering
  \begin{footnotesize}
  \caption{Protocolo de pesquisa: estratégia de busca.}
  \begin{tabular}{|c|c|}
  \hline
    Termos & \textit{multi-agent planning} \\ \hline
    \multirow{5}{*}{Fontes} & \textit{ACM Digital Library}\footnotemark[1] \\ \cline{2-2}
    & \textit{IEEExplore Digital Library}\footnotemark[2] \\ \cline{2-2}
    & \textit{Springer Link}\footnotemark[3] \\ \cline{2-2}
    & \textit{International Conference on Automated Planning and Scheduling (ICAPS)}\footnotemark[4] \\ 
  \cline{2-2}
    & A partir de referências de outros trabahos \\ \hline
  \multirow{3}{*}{Filtros} & Artigos \\ \cline{2-2}
    & Periódicos \\ \cline{2-2}
    & Publicados entre 2011 e 2017 \\ \hline
  \end{tabular}
  \label{cap2:estratégia}
  \vspace{-6mm}
 \end{footnotesize}
\end{table}

Em relação aos dados a serem obtidos de cada estudo incluído destacam-se o ano e o veículo de publicação, o índice de qualidade 
(QUALIS) do veículo, a principal contribuição do trabalho e o tipo de domínio de acoplamento, isto é, fraco ou forte.

Na fase de execução, foram realizadas buscas nas fontes de pesquisas de acordo com termos e filtros destacados na Tabela 
\ref{cap2:estratégia}. O resultado final da atividade de identificação de pesquisa é resumido na Tabela \ref{cap2:trabalhos}, com 
os valores organizados por fonte.

\footnotetext[1]{http://dl.acm.org/}
\footnotetext[2]{http://ieeexplore.ieee.org/Xplore/home.jsp}
\footnotetext[3]{https://link.springer.com/}
\footnotetext[4]{http://www.aaai.org/Library/ICAPS/icaps-library.php}


\begin{table}[htbp]
 \centering
%  \begin{footnotesize}
 \caption{Identificação de pesquisa.}
 \begin{tabular}{|c|c|c|c|}
  \hline
  Fonte & Incluídos & Excluídos & Total \\ \hline
  \textit{IEEExplore Digital Library} & 14 & 26 & 40 \\ \hline
  \textit{ACM Digital Library} & 6 & 61 & 67 \\ \hline
  \textit{Springer Link} & 16 & 160 & 176 \\ \hline
  ICAPS & 5 & 2 & 7 \\ \hline
  A partir de outros trabalhos & 2 & 0 & 2 \\ \hline
  Geral & 43 & 249 & 292 \\ \hline
 \end{tabular}
 \label{cap2:trabalhos}
%  \end{footnotesize}
\end{table}

Para a atividade seguinte de avaliação de qualidade, apesar do total de trabalhos incluídos ser de 43, a quantidade de trabalhos 
que foram avaliados de acordo com o critério de qualidade foi de 35. A razão pela diferença foi o fato de 8 trabalhos resultantes 
da busca na base da \textit{Springer Link} estarem parcialmente disponíveis, isto é, apresentavam basicamente o resumo. Os outros 
trabalhos, mesmo apresentando a mesma limitação, estavam disponíveis em outras plataformas o que permitiu as avaliações.

Em relação ao critério de qualidade utilizado para avaliar os trabalhos incluídos, foi elaborado um conjunto de 6 perguntas cujas 
respostas eram sim ou não, listadas na Tabela \ref{cap2:perguntas}. A menção final de cada trabalho era calculada como a 
quantidade de respostas afirmativas. A Tabela \ref{cap2:qualidade} apresenta os 35 trabalhos incluídos e destaca aqueles cujas 
menções finais são maiores ou iguais ao valor 3.

Os trabalhos que foram destacados com as maiores menções de qualidades tiveram seus dados extraídos, segundo a Tabela 
\ref{cap2:extracao}. Nessa sumarização, a principal motivação foi identificar qual é a principal contribuição do trabalho e se 
assume algum tipo de restrição quanto ao nível de acoplamento. Trabalhos que podem ser utilizados independente do tipo de 
domínio, seja ele fraca ou fortemente acoplado, oferecem uma maior possibilidade de uso.

\begin{table}[htbp]
 \centering
 \caption{Conjunto de perguntas do critério de avaliação.}
 \begin{tabular}{|c|c|} 
  \hline
  Item & Pergunta \\ \hline
  Q1 & O estudo discute a corretude e/ou completude do algoritmo/modelo proposto? \\ \hline
  Q2 & O estudo discute a escalabilidade do modelo? \\ \hline
  Q3 & O estudo apresenta avaliação de resultados e comparação com outros trabalhos? \\ \hline
  Q4 & Os teste realizados utilizam os domínios do IPC? \\ \hline
  Q5 & O modelo utiliza um linguagem padrão (PDDL, ADL, STRIPS)? \\ \hline
  Q6 & Existe alguma implementação executável disponível para reproduzir os resultados? \\ \hline
  Q7 & Existe alguma seleção de agentes antes do planejamento? \\ \hline
  Q8 & Algum tipo de verficação do problema é realizada antes do planejamento? \\ \hline
 \end{tabular}
 \label{cap2:perguntas}
\end{table}

A partir análise das respostas referentes ao critério de avaliação é possível destacar algumas observações sobre o estado da arte 
da área de pesquisa de planejamento multiagente. 

Discussões sobre corretude e completude dos algoritmos e escalabilidade do modelo são raramente apresentadas nos trabalhos, 
atingindo coberturas de $11.43\%$ e $17.14\%$, respectivamente. 

Considerando os aspecto de avaliação de desempenho, 1 em cada 5 propostas apresenta uma comparação com outros trabalhos, o que 
auxilia na discussão das contribuições, vantagens e desvantagens. Os maiores níveis de cobertura estão relacionados aos casos de 
testes e à linguagem utilizada, indicando que $25.71\%$ dos trabalhos utilizam \textit{benchmarks} do IPC 
\citep{hoffmann2006engineering} e que $31.43\%$ utilizam alguma linguagem padrão para descrever o problema de planejamento. 
Especialmente no aspecto de linguagem, os demais trabalhos ou não explicitavam qual padronização seguiam ou realizam extensões nas 
linguagens definindo termos próprios para marcar alguns tipos de informações. 

Dos itens incluídos, apenas 2 trabalhos disponibilizam (na Internet) implementações de seus modelos de forma a permitir a 
reprodução dos experimentos, execução de novos testes e principalmente comparação de resultados.

Por fim, etapas de seleção de agentes ou verificação do problema antes do início do processo de planejamento são fatos raros nos 
trabalhos, estando presentes apenas em um trabalho. 

Os resultados consolidados na atividade de sumarização foram utilizados para definir os principais trabalhos da área de 
planejamento multiagente. Dessa forma, a fase de publicação dessa revisão sistemática da literatura foi caracterizada 
pelo detalhamento dos trabalhos correlatos presentes nessa dissertação.

Portanto, no Capítulo \ref{cap:trabalhosCorrelatos} foi dada ênfase aos trabalhos que obtiveram as maiores menções de 
qualidade. Esse conjunto de propostas forma a base dos estudos que motivaram e justificaram as decisões tomadas para definição do 
modelo proposta nessa dissertação. No entanto, o fato de uma proposta, referenciada por outros trabalhos, não pertencer a tal 
conjunto não exclui a possibilidade nem a necessidade de avaliá-la, mesmo que de maneira secundária. 

Como resultado paralelo da revisão sistemática foi possível sugerir uma taxonomia de classificação dos trabalhos correlatos, 
destacando características importantes como exploração de técnicas de execução paralela, avaliação do problema antes do início do 
planejamento, seleção de agentes e tipo de abordagem, isto é, centralizada ou distribuída.



% \textbf{Fazer o link para seção de revisão da literatura explicando quais trabalhos formam a base e pq foram escolhidas. Destacar 
% a escolha dos trabalhos q apresentam implemntacao disponivel. Citar tbm outros pontos que foram destacados nos demais trabalhos, 
% paralelização, falta do teste de solvabilidade para fazer o link com o framework apresentado.}

% \textbf{REFAZER A PARTE DE TRABALHOS CORRELATOS. RETIRAR DOMAPS e SINGLE AGENT TO MAP}

\begin{landscape}
    \begin{table} [!htp]
    \begin{footnotesize}
    \centering
    \caption{Avaliação de qualidade dos trabalhos incluídos.}
      \begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}
	\hline
	
	Item & Trabalho & Q1 & Q2 & Q3 & Q4 & Q5 & Q6 & Q7 & Q8 & Total \\ \hline
	1 & A Collaborative Multiagent Framework Based on Online Risk-Aware Planning and Decision-Making &N&N&N&N&N&N&N&N&0 \\ 
\hline
	2&A decentralized approach to multi-agent planning in the presence of constraints and uncertainty&N&N&N&N&N&N&N&N&0\\ 
\hline
	3&A flexible coupling approach to multi-agent planning under incomplete information&N&S&N&N&N&N&N&N&1\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}4&A Formal Analysis of Required Cooperation in Multi-Agent Planning&S&N&S&N&S&N&N&S&4\\ \hline
	5&Allocating Social Goals Using the Contract Net Protocol in Online Multi-agent Planning&N&N&N&N&N&N&N&N&0\\ \hline
	6&An extension of FMAP for joint actions&N&N&N&N&N&N&N&N&0\\ \hline
	7&Argumentation Schemes for Collaborative Planning&N&N&N&N&N&N&N&N&0\\ \hline
	8&A Two-Stage Online Approach for Collaborative Multi-agent Planning Under Uncertainty&N&N&N&N&N&N&N&N&0\\ \hline
	9&Defeasible Argumentation for Multi-agent Planning in Ambient Intelligence Applications&N&N&N&N&S&N&N&N&1\\ \hline
	10&Divide \& conquer in planning for self-optimizing mechatronic systems - A first application 
example&N&N&N&N&N&N&N&N&0\\ 
  \hline
	11&Find a Multi-agent planning solution in nondeterministic domain &N&N&N&N&N&N&N&N&0\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}12&FMAP: Distributed cooperative multi-agent planning &S&S&S&S&N&S&N&N&5\\ \hline
	13&Global Heuristics for Distributed Cooperative Multi-Agent Planning&N&S&S&N&N&N&N&N&2\\ \hline
	14&Group planning with time constraints&N&N&N&N&N&N&N&N&0\\ \hline
	15&Hybrid mission planning with coalition formation&N&N&N&S&N&N&N&N&1\\ \hline
	16&Hybrid Multi-agent Planning &N&N&N&S&N&N&N&N&1\\ \hline
	17&Increased Privacy with Reduced Communication in Multi-Agent Planning&S&N&N&S&N&N&N&N&2\\ \hline
	18&Lagrangian Relaxation for Large-Scale Multi-agent Planning&N&S&N&N&N&N&N&N&1\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}19&MAPJA: Multi-agent planning with joint actions&S&N&S&N&S&N&N&N&3\\ \hline
	20&Multi-agent A* for Parallel and Distributed Systems&N&N&N&N&N&N&N&N&0\\ \hline
	21&Multiagent Argumentation for Cooperative Planning in DeLP-POP&N&N&N&N&N&N&N&N&0\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}22&Multi-agent Planning by Plan Reuse&N&N&S&S&S&N&N&N&3\\ \hline
	23&Multi-agent planning for fleet cooperative air defense decision-making&N&S&N&N&S&N&N&N&2\\ \hline
	24&Multi-agent planning with joint action&N&N&N&N&S&N&N&N&1\\ \hline
	25&Multi-agent planning with quantitative capability&N&N&N&S&S&N&N&N&2\\ \hline
	26&On the Construction of Joint Plans Through Argumentation Schemes&N&N&N&N&N&N&N&N&0\\ \hline
	27&Potential Heuristics for Multi-Agent Planning&N&N&N&S&S&N&N&N&2\\ \hline
	28&Robust Plan Execution in Multi-agent Environments&N&N&N&N&N&N&N&N&0\\ \hline
	29&Scalable, MDP-based planning for multiple, cooperating agents&N&N&N&N&N&N&N&N&0\\ \hline
	30&Secure Multi-Agent Planning&N&N&N&N&N&N&N&N&0\\ \hline
	31&Social Continual Planning in Open Multiagent Systems: A First Study &N&N&N&N&N&N&N&N&0\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}32&Transforming Multi-agent Planning Into Single-Agent Planning Using Best-Cost 
Strategy&N&S&S&N&S&N&N&N&3\\ \hline
	33&Validation and Verification of Joint-Actions in Multi-agent Planning&N&N&N&N&N&N&N&S&1\\ \hline
	34&Psm-based planners description for codmap 2015 competition&N&N&N&S&S&N&N&N&2\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}35&MADLA: planning with distributed and local search&N&N&S&S&S&S&N&N&4\\ \hline	
      \end{tabular}
      \end{footnotesize}
      \label{cap2:qualidade}
    \end{table}
\end{landscape}

\begin{landscape}
  \begin{table}
  \centering
  \caption{Sumarização dos principais trabalhos.}
  \begin{tabular}{|>{\centering}p{6cm}|c|c|c|>{\centering}p{6cm}|c|}
    \hline
    \multirow{2}{*}{Trabalho} & \multicolumn{2}{c|}{Publição} & Índice & \multirow{2}{*}{Contribuição} & 
    \multirow{2}{*}{Acomplamento} \\ \cline{2-3}
      & Ano & Veículo & QUALIS &  &  \\ \hline
    A Formal Analysis of Required Cooperation in Multi-Agent Planning & 2016 & ICAPS & A2 & Algoritmo para determinar se é 
preciso haver cooperação entre agentes para atingir objetivos & Ambos \\ \hline
    FMAP: Distributed cooperative multi-agent planning & 2014 & \textit{Applied Intelligence} & B1 & Planejador independente de 
    domínio utilizando a técnica de refinamento de planos & Ambos \\ \hline
    MADLA: planning with distributed and local search & 2015 & AAAI & A1 & Planejador multiagente distribuído ou centralizado & 
  Ambos \\ \hline
    MAPJA: Multi-agent planning with joint actions & 2017 & Applied Intelligence & B1 & Planejador multiagente independente de 
  domínio e capaz de resolver ações conjuntas &  Ambos\\ \hline
    Multi-agent Planning by Plan Reuse  & 2013 & AAMAS & A1 & Distribuição de objetivos entre os agentes & Fraco  \\ \hline
    Transforming Multi-agent Planning Into Single-Agent Planning Using Best-Cost Strategy & 2016 & BRACIS & B2 & Análise do ganho 
  com distribuição de objetivos & Fraco \\ \hline
  \end{tabular}
  \label{cap2:extracao}
  \end{table}
\end{landscape}

