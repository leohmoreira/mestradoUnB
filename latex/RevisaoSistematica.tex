Após a definição do problema a ser investigado em uma pesquisa, uma prática importante é a revisão da literatura.
Segundo \cite{kitchenham2004}, uma revisão sistemática da literatura é um meio de identificar, avaliar e interpretar todas as 
pesquisas disponíveis relevantes para uma determinada questão de pesquisa, área de tópico ou fenômeno de interesse. Entre os 
motivos que justificam a importância de sua realização, destacam-se a identificação de trabalhos correlatos, a investigação de 
lacunas e oportunidades de melhorias e o posicionamento adequado de novas atividades na área de pesquisa. Para facilitar a 
execução da revisão foi utilizada uma ferramenta disponível em \url{http://sesra.net}.

Em relação as principais características de uma revisão sistemática, pode-se citar:
\begin{itemize}
 \item começa pela definição de um protocolo que especifica a questão de pesquisa e os métodos que serão utilizados;
 \item baseia-se em uma estratégia de busca para encontrar o maior número de trabalhos relacionados possível;
 \item registra essa estratégia de forma a permitir sua reprodução por outros pesquisadores;
 \item exige critérios claros para inclusão e exclusão do trabalhos identificados;
 \item especifica quais informações serão buscadas de cada trabalho e como serão qualificadas;
 \item é executada em três fases: planejamento, execução e publicação.
\end{itemize}

Na fase de planejamento é verificada a existência de alguma revisão sistemática relacionado à área de pesquisa e com critérios 
semelhantes de avaliação. Dessa forma, se houver trabalhos relacionados já publicados, estes deverão ser utilizados. Por outro 
lado, na ausência deles, um protocolo de revisão deve ser elaborado para permitir a realização da revisão de maneira imparcial, 
sem vícios e livre de direcionamentos frutos de expectativas individuais dos pesquisadores. De maneira geral, nesse protocolo são 
definidos a questão de pesquisa, a estratégia de busca incluindo os termos e fontes de buscas, os critérios de seleção dos 
trabalhos, a avaliação de qualidade dos trabalhos incluídos, o conjunto de informações a ser obtido de cada item selecionado, a 
síntese dos dados obtidos e um cronograma de execução.

A questão de pesquisa destaca os principais tópicos a serem investigados e sua estrutura considera os seguinte pontos:
\begin{itemize}
 \item População: quem ou qual será foco da pesquisa;
 \item Intervenção: aspectos específicos a serem pesquisados;
 \item Resultados: o que se deseja atingir;
 \item Contexto: quais são as circunstâncias mais importantes da pesquisa.
\end{itemize}
 
Na fase de execução e com o protocolo já definido, a revisão sistemática é realizada em cinco etapas. Primeiramente, a estratégia 
de busca é definida pela escolha dos termos (palavras-chaves) e fontes de pesquisa (periódicos, conferências etc). De posse dos 
trabalhos obtidos, na segunda fase é realizada a seleção dos estudos seguindo os critérios de inclusão e exclusão, identificando 
aqueles potencialmente relevantes à pesquisa. Em seguida, os itens escolhidos são qualificados seguindo um critério definido pelo 
pesquisador a fim de organizá-los de acordo com importância e relevância. Nessa terceira fase de avaliação de qualidade, o 
critério a ser seguido é composto, preferencialmente, por um conjunto de perguntas respondidas por sim ou não. Consequentemente, 
um número maior de respostas afirmativas implica em um grau maior de qualidade. Na quarta etapa, dados específicos são obtidos 
de cada estudo buscando responder as questões de pesquisa. Por fim, os dados são sumarizados em forma de gráficos ou tabelas.

Na fase de publicação, os resultados da revisão sistemática podem ser compartilhados tanto na forma de um trabalho apresentado em 
periódicos e conferências quanto como parte de uma dissertação ou tese.

A fim de resumir a ordem cronológica das tarefas necessárias para a realização de uma revisão sistemática, a 
Tabela \ref{cap2:cronograma} apresenta as atividades que foram realizadas na aplicação da técnica nessa dissertação.

\begin{table}[!htp]
 \centering
 \caption{Fases e atividades de uma revisão sistemática.}
 \begin{tabular}{|l|l|}
  \hline
  Fase &  Atividades \\ \hline
  \multirow{3}{*}{Planejamento} & Identificar a necessidade de revisão \\ \cline{2-2}
& Especificar questão de pesquisa \\ \cline{2-2}
& Desenvolver o protocolo da revisão \\ \hline
  \multirow{5}{*}{Execução}     & Identificar pesquisa \\ \cline{2-2}
& Selecionar os estudos primários \\ \cline{2-2}
& Avaliar a qualidade dos estudos \\ 
& (Proximidade com a questão de pesquisa) \\ \cline{2-2}
& Extrair e monitorar os dados \\ \cline{2-2}
& Sintetizar os dados \\ \hline
  \multirow{3}{*}{Publicação}   & Especificar mecanismo de disseminação \\ \cline{2-2}
& Formatar o relatório principal \\ \cline{2-2} 
& Validar o relatório \\ \hline
 \end{tabular}
 \label{cap2:cronograma}
\end{table}

\section{Aplicação}

Seguindo o roteiro sugerido por \cite{kitchenham2004} e apresentado na Tabela \ref{cap2:cronograma}, a primeira atividade 
realizada foi a identificação da necessidade de uma revisão na área de pesquisa de planejamento multiagente. Nesse 
sentido, foram realizadas buscas em veículos de publicação de trabalhos científicos como \textit{ACM Digital Library, IEEExplore 
Digital Library, Springer, Google Scholar}. 

Cabe ressaltar que visando abranger a maior quantidade possível de 
trabalhos, os termos utilizados nas buscas foram definidos na língua inglesa, uma vez que grande parte dos trabalhos são 
publicados nesse idioma.

% \subsection{Questão de pesquisa}

A questão central da revisão sistemática bem como sua estrutura foram definidas conforme apresentado a seguir:

\begin{itemize}
 \item Questão de pequisa: Quais são os efeitos da execução de um pré-planejamento para um planejamento multiagente?
 \item População: \textit{Multi-agent Systems, Automated Planning, Multi-agent Planning};
 \item Intervenção: modelos, algoritmos e abordagens que tratam de seleção de agentes, validação e verificação em planejamento 
multiagente;
 \item Resultados: volume menor de mensagens trocadas entre os agentes e tempo gasto durante o processo de planejamento em 
comparação com trabalhos que configuram o estado da arte de planejamento multiagente;
 \item Contexto: \textit{Multi-agent Systems, Automated Planning, Multi-agent Planning}.
\end{itemize}

Com a questão já definida, a definição do protocolo de pesquisa foi iniciada pela estratégia de busca. O termo, as fontes e os 
filtros utilizados nas pesquisas são apresentados na Tabela \ref{cap2:estratégia}. O termo \textit{``multi-agent planning''} foi 
aplicado em qualquer campo, não sendo limitado ao título ou resumo dos trabalhos, incluídos em qualquer parte do texto do 
trabalho. Além disso, as buscas foram realizadas durante o período de Fevereiro a Junho de 2017.

A condição para inclusão dos resultados das pequisas foi definida por dois critérios: domínio de aplicação e menção do veículo 
de publicação. Para ser considerado relevante, o trabalho deveria apresentar modelos ou algoritmos independentes de 
domínio e aplicáveis em planejamento multiagente. De maneira complementar, apenas os artigos publicados em veículos com 
índices QUALIS CAPES maiores ou iguais a B2 foram incluídos.

Dessa forma, os trabalhos que não focassem em planejamento multiagente ou que fossem dependentes de domínio de aplicação foram 
excluídos. Essa condição foi necessária para ignorar trabalhos com foco em cenários restritos como aplicação em tráfego aéreo, 
manutenção da privacidade das informações dos agentes, composição de \textit{web services} e planejamento de rotas (\textit{path 
planning}). Portanto, trabalhos com maior abrangência e independente de domínio foram priorizados.

\begin{footnotesize}
\begin{table}[htp]
  \centering
  \caption{Protocolo de pesquisa: estratégia de busca.}
  \begin{tabular}{|c|l|}
  \hline
    Termo & \textit{``multi-agent planning''} \\ \hline
    \multirow{5}{*}{Fontes} & \textit{ACM Digital Library}\footnotemark[1] \\ \cline{2-2}
    & \textit{IEEE Xplore Digital Library}\footnotemark[2] \\ \cline{2-2}
    & \textit{Springer Link}\footnotemark[3] \\ \cline{2-2}
    & \textit{International Conference on Automated Planning and Scheduling (ICAPS)}\footnotemark[4] \\ 
  \cline{2-2}
    & Outras - a partir de referências importantes de outros trabalhos \\ \hline
  \multirow{3}{*}{Filtros} & Artigos \\ \cline{2-2}
    & Periódicos \\ \cline{2-2}
    & Publicados entre 2011 e 2017 \\ \hline  
  \end{tabular}
  \label{cap2:estratégia}
\end{table}
\end{footnotesize}

Em relação aos dados a serem obtidos de cada estudo incluído destacam-se o ano e o veículo de publicação, o índice de qualidade 
(QUALIS CAPES) do veículo, a principal contribuição do trabalho e o tipo de acoplamento, isto é, fraco ou forte.

Na fase de execução, foram realizadas buscas nas fontes de pesquisas de acordo com repositórios e filtros destacados na Tabela 
\ref{cap2:estratégia}. O resultado final da atividade de identificação de pesquisa é resumido na Tabela \ref{cap2:trabalhos}, com 
os valores organizados por fonte.

\footnotetext[1]{http://dl.acm.org/}
\footnotetext[2]{http://ieeexplore.ieee.org/Xplore/home.jsp}
\footnotetext[3]{https://link.springer.com/}
\footnotetext[4]{http://www.aaai.org/Library/ICAPS/icaps-library.php}

\begin{table}[htbp]
 \centering
%  \begin{footnotesize}
 \caption{Identificação de pesquisa.}
 \begin{tabular}{|l|r|r|r|r|r|r|}
  \hline
  Fonte & \multicolumn{2}{c|}{Incluídos} & \multicolumn{2}{c|}{Excluídos}&  \multicolumn{2}{c|}{Total} \\ \hline
  \textit{IEEExplore Digital Library} & 14 & 32,56\% & 26 &10,44\% & 40 & 13,7\% \\ \hline
  \textit{ACM Digital Library} & 6 & 13,95\% & 61 & 24,5\% & 67 &22,94\% \\ \hline
  \textit{Springer Link} & 16 & 37,21\% & 160 & 64,26\% & 176 & 60,27\% \\ \hline
  ICAPS & 5 & 11,63\% & 2 & 0,8\% & 7 & 2,4\%\\ \hline
  Outras fontes & 2 & 4,65\%& 0 & 0\% & 2 & 0,7\%\\ \hline
  Geral & 43 & 100\% & 249 & 100\% & 292 & 100\%\\ \hline
 \end{tabular}
 \label{cap2:trabalhos}
%  \end{footnotesize}
\end{table}

Realizando uma análise estatística dos resultados da Tabela \ref{cap2:trabalhos} é possível perceber que apesar de fornecer mais 
de 60\% dos trabalhos (176 de um total de 292), a fonte \textit{Springer Link} contribui apenas com $37,21 \%$ do total itens 
incluídos. Por outro lado, a fonte \textit{IEEExplore Digital Libray} que forneceu $13,7\%$ dos trabalhos,  
contribui com $32,56\%$ na fase de inclusão. Os maiores índices de aceitação foram obtidos da fonte \textit{ICAPS} e outras 
fontes, fato que já era esperado pois configuram bases de busca dedicadas à área de planejamento. Os 
detalhes dessa análise estão apresentados na Figura \ref{fig:analise}, na qual são apresentados o total de itens resultantes da 
busca em cada fonte (Figura \ref{fig:analise}(a)), a porcentagem de inclusão e exclusão de cada fonte (Figura 
\ref{fig:analise}(b)) e a contribuição final de cada fonte para o trabalhos incluídos (Figura \ref{fig:analise}(c)).

\begin{figure}[ht]
 \centering
 \subfigure[Trabalhos identificados por fonte.]{\includegraphics[width=0.45\textwidth]{images/basesPie.png}}
 \subfigure[Inclusão e exclusão por fonte.]{\includegraphics[width=0.45\textwidth]{images/incluidosExcluidos.png}}
 \subfigure[Contribuição para trabalhos incluídos.]{\includegraphics[width=0.45\textwidth]{images/incluidosPie.png}}
 \caption{Análise dos resultados de identificação de pesquisa.}
 \label{fig:analise}
\end{figure}

Para a atividade seguinte de avaliação da proximidade dos trabalhos com a questão de pesquisa definida nesta dissertação, apesar 
do total de trabalhos incluídos ser de 
43, a quantidade de trabalhos que foram avaliados de acordo com o critério de qualidade foi de 35. A razão pela diferença foi o 
fato de oito trabalhos 
resultantes 
da busca na base da \textit{Springer Link} estarem parcialmente disponíveis, isto é, apresentavam basicamente o resumo. Os outros 
trabalhos, mesmo apresentando a mesma limitação, estavam disponíveis em outras plataformas o que permitiu as avaliações.

Em relação ao critério de qualidade utilizado para avaliar os trabalhos incluídos, foi elaborado um conjunto de seis perguntas 
cujas respostas eram sim ou não, listadas na Tabela \ref{cap2:perguntas}. A menção final de cada trabalho era calculada através 
da 
soma de respostas afirmativas. A Tabela \ref{cap2:qualidade} apresenta os 35 trabalhos incluídos e destaca aqueles 
cujas menções finais são maiores ou iguais ao valor três. Esse valor foi estipulado a partir da média das menções dos trabalhos 
que apresentaram pelo menos uma resposta positiva, ou seja, $\lceil{\frac{42}{19}}\rceil$. 

Os trabalhos que foram destacados com as maiores proximidades com a questão de pesquisa tiveram seus dados extraídos, segundo a 
Tabela \ref{cap2:extracao} e ordenados pelo índice QUALIS CAPES\footnote{Plataforma 
Sucupira da CAPES - \url{https://sucupira.capes.gov.br}}. Nessa sumarização, a principal motivação foi identificar qual é 
a principal contribuição do trabalho e se 
assume algum tipo de restrição quanto ao nível de acoplamento. Assume-se como hipótese que trabalhos que podem ser 
utilizados independente do tipo de domínio, seja ele fraca ou fortemente acoplado, oferecem uma maior possibilidade de uso.

\begin{table}[htbp]
 \centering
 \caption{Conjunto de perguntas do critério de avaliação.}
 \begin{tabular}{|c|l|} 
  \hline
  Item & Pergunta \\ \hline
  Q1 & O estudo discute a corretude e/ou completude do algoritmo/modelo proposto? \\ \hline
  Q2 & O estudo discute a escalabilidade do modelo? \\ \hline
  Q3 & O estudo apresenta avaliação de resultados e comparação com outros trabalhos? \\ \hline
  Q4 & Os teste realizados utilizam os domínios do IPC? \\ \hline
  Q5 & O modelo utiliza um linguagem padrão (PDDL, ADL, STRIPS)? \\ \hline
  Q6 & Existe alguma implementação executável disponível para reproduzir os resultados? \\ \hline
  Q7 & Existe alguma seleção de agentes antes do planejamento? \\ \hline
  Q8 & Algum tipo de verificação do problema é realizada antes do planejamento? \\ \hline
 \end{tabular}
 \label{cap2:perguntas}
\end{table}

A partir da análise das respostas positivas referentes ao critério de avaliação descrito na Tabela \ref{cap2:perguntas} é 
possível destacar algumas observações sobre o estado da arte da área de planejamento multiagente. 
Discussões sobre corretude e completude dos algoritmos e escalabilidade do modelo são raramente apresentadas nos trabalhos, 
atingindo coberturas de $11.43\%$ e $17.14\%$, respectivamente. 

Considerando os aspectos de avaliação de desempenho, um em cada cinco propostas apresenta uma comparação com outros trabalhos, o 
que possibilita a discussão das contribuições, vantagens e desvantagens. Os maiores níveis de cobertura estão relacionados aos 
casos de testes e à linguagem utilizada, indicando que $25.71\%$ dos trabalhos utilizam \textit{benchmarks} do IPC 
\citep{hoffmann2006engineering} e que $31.43\%$ utilizam alguma linguagem padrão para descrever o problema de planejamento. 
Especialmente no aspecto de linguagem, os demais trabalhos ou não explicitavam qual padronização seguiam ou realizam extensões 
nas 
linguagens definindo termos próprios para marcar alguns tipos de informações. 

Dos itens incluídos, apenas dois trabalhos disponibilizam (na Internet) implementações de seus modelos de forma a permitir a 
reprodução dos experimentos, execução de novos testes e principalmente comparação de resultados \cite{fmap}, \cite{madla}.

Por fim, etapas de seleção de agentes ou verificação do problema antes do início do processo de planejamento é uma lacuna não 
preenchida, tendo sido detectada apenas em dois trabalhos \cite{cooperation}, \cite{nath2015validation}.
% são fatos raros nos 
% trabalhos, estando presentes apenas em um trabalho. 

Os resultados consolidados na atividade de sumarização foram utilizados para definir os principais trabalhos da área de 
planejamento multiagente. Dessa forma, a fase de publicação da revisão sistemática da literatura foi caracterizada 
pelo detalhamento dos trabalhos correlatos presentes nessa dissertação.

Portanto, no Capítulo \ref{cap:trabalhosCorrelatos} foi dada ênfase aos trabalhos que obtiveram as maiores menções de 
qualidade. Esse conjunto de propostas forma a base dos estudos que motivaram e justificaram as decisões tomadas para definição do 
modelo proposta nessa dissertação. No entanto, o fato de uma proposta, referenciada por outros trabalhos, não pertencer a tal 
conjunto não exclui a possibilidade nem a necessidade de avaliá-la, mesmo que de maneira secundária. 

Como resultado paralelo da revisão sistemática foi possível sugerir uma taxonomia de classificação dos trabalhos correlatos, 
destacando características importantes como exploração de técnicas de execução paralela, avaliação do problema antes do início do 
planejamento, seleção de agentes e tipo de abordagem de planejamento, isto é, centralizada ou distribuída.



% \textbf{Fazer o link para seção de revisão da literatura explicando quais trabalhos formam a base e pq foram escolhidas. 
% Destacar 
% a escolha dos trabalhos q apresentam implemntacao disponivel. Citar tbm outros pontos que foram destacados nos demais 
% trabalhos, 
% paralelização, falta do teste de solvabilidade para fazer o link com o framework apresentado.}

% \textbf{REFAZER A PARTE DE TRABALHOS CORRELATOS. RETIRAR DOMAPS e SINGLE AGENT TO MAP}

\begin{landscape}
    \begin{table} [!htp]
    \begin{footnotesize}
    \centering
    \caption{Avaliação de qualidade dos trabalhos incluídos.}
      \begin{tabular}{|c|l|c|c|c|c|c|c|c|c|c|}
	\hline
	
	Item & Trabalho & Q1 & Q2 & Q3 & Q4 & Q5 & Q6 & Q7 & Q8 & Total \\ \hline
	1 & A Collaborative Multiagent Framework Based on Online Risk-Aware Planning and Decision-Making 
\cite{palomares2016collaborative}&N&N&N&N&N&N&N&N&0 \\ 
\hline
	2&A decentralized approach to multi-agent planning in the presence of constraints and uncertainty 
\cite{undurti2011decentralized}&N&N&N&N&N&N&N&N&0\\ 
\hline
	3&A flexible coupling approach to multi-agent planning under incomplete information 
\cite{torreno2014flexible}&N&S&N&N&N&N&N&N&1\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}4&A Formal Analysis of Required Cooperation in Multi-Agent Planning 
\cite{cooperation}&S&N&S&N&S&N&S&S&5\\ \hline
	5&Allocating Social Goals Using the Contract Net Protocol in Online Multi-agent Planning 
\cite{bracis_CNP}&N&N&N&N&N&N&N&N&0\\ \hline
	6&An extension of FMAP for joint actions \cite{extensionfmap}&N&N&N&N&N&N&N&N&0\\ \hline
	7&Argumentation Schemes for Collaborative Planning \cite{toniolo2011argumentation}&N&N&N&N&N&N&N&N&0\\ \hline
	8&A Two-Stage Online Approach for Collaborative Multi-agent Planning Under Uncertainty 
\cite{palomares2016two}&N&N&N&N&N&N&N&N&0\\ \hline
	9&Defeasible Argumentation for Multi-agent Planning in Ambient Intelligence 
Applications\cite{ferrando2012defeasible}&N&N&N&N&S&N&N&N&1\\ \hline
	10&Divide \& conquer in planning for self-optimizing mechatronic systems - A first application 
example \cite{klopper2011divide}&N&N&N&N&N&N&N&N&0\\ 
  \hline
	11&Find a Multi-agent planning solution in nondeterministic domain \cite{chang2012find}&N&N&N&N&N&N&N&N&0\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}12&FMAP: Distributed cooperative multi-agent planning \cite{fmap}&S&S&S&S&N&S&N&N&5\\ \hline
	13&Global Heuristics for Distributed Cooperative Multi-Agent Planning \cite{torreno2015global}&N&S&S&N&N&N&N&N&2\\ \hline
	14&Group planning with time constraints \cite{hadad2013group}&N&N&N&N&N&N&N&N&0\\ \hline
	15&Hybrid mission planning with coalition formation\cite{dukeman2017hybrid}&N&N&N&S&N&N&N&N&1\\ \hline
	16&Hybrid Multi-agent Planning \cite{elkawkagy2011hybrid} &N&N&N&S&N&N&N&N&1\\ \hline
	17&Increased Privacy with Reduced Communication in Multi-Agent Planning\cite{ICAPS1715739}&S&N&N&S&N&N&N&N&2\\ \hline
	18&Lagrangian Relaxation for Large-Scale Multi-agent Planning \cite{gordon2012lagrangian}&N&S&N&N&N&N&N&N&1\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}19&MAPJA: Multi-agent planning with joint actions\cite{mapja}&S&N&S&N&S&N&S&N&4\\ \hline
	20&Multi-agent A* for Parallel and Distributed Systems \cite{nissim2012multi}&N&N&N&N&N&N&N&N&0\\ \hline
	21&Multiagent Argumentation for Cooperative Planning in DeLP-POP \cite{pardo2011multiagent}&N&N&N&N&N&N&N&N&0\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}22&Multi-agent Planning by Plan Reuse \cite{mapr}&N&N&S&S&S&N&N&N&3\\ \hline
	23&Multi-agent planning for fleet cooperative air defense decision-making \cite{wang2013multi}&N&S&N&N&S&N&N&N&2\\ \hline
	24&Multi-agent planning with joint action \cite{chouhan2015multi}&N&N&N&N&S&N&N&N&1\\ \hline
	25&Multi-agent planning with quantitative capability\cite{chouhan2016multi}&N&N&N&S&S&N&N&N&2\\ \hline
	26&On the Construction of Joint Plans Through Argumentation Schemes\cite{sapena2011construction}&N&N&N&N&N&N&N&N&0\\ 
\hline
	27&Potential Heuristics for Multi-Agent Planning\cite{vstolba2016potential}&N&N&N&S&S&N&N&N&2\\ \hline
	28&Robust Plan Execution in Multi-agent Environments\cite{guzman2014robust}&N&N&N&N&N&N&N&N&0\\ \hline
	29&Scalable, MDP-based planning for multiple, cooperating agents\cite{redding2012scalable}&N&N&N&N&N&N&N&N&0\\ \hline
	30&Secure Multi-Agent Planning\cite{vstolba2016secure}&N&N&N&N&N&N&N&N&0\\ \hline
	31&Social Continual Planning in Open Multiagent Systems: A First Study \cite{baldoni2015social}&N&N&N&N&N&N&N&N&0\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}32&Transforming Multi-agent Planning Into Single-Agent Planning Using Best-Cost 
Strategy\cite{bracis}&N&S&S&N&S&N&N&N&3\\ \hline
	33&Validation and Verification of Joint-Actions in Multi-agent Planning\cite{nath2015validation}&N&N&N&N&N&N&N&S&1\\ 
\hline
	34&Psm-based planners description for codmap 2015 competition\cite{tozicka2015psm}&N&N&N&S&S&N&N&N&2\\ \hline
	\rowcolor[rgb]{0.8,0.8,0.8}35&MADLA: planning with distributed and local search\cite{madla}&N&N&S&S&S&S&N&N&4\\ \hline	
      \end{tabular}
      \end{footnotesize}
      \label{cap2:qualidade}
    \end{table}
\end{landscape}

\begin{landscape}
  \begin{table}
  \centering
  \caption{Sumarização dos principais trabalhos.}
%   \begin{tabular}{|>{\centering}p{6cm}|c|c|c|>{\centering}p{6cm}|c|}
  \begin{tabular}{|p{6cm}|c|c|c|p{6cm}|c|}
    \hline
    \multirow{2}{*}{Trabalho} & \multicolumn{2}{c|}{Publição} & Índice & \multirow{2}{*}{Contribuição} & 
    \multirow{2}{*}{Acomplamento} \\ \cline{2-3}
      & Ano & Veículo & QUALIS &  &  \\ \hline
    MADLA: planning with distributed and local search (\cite{madla})& 2015 & AAAI & A1 & Planejador multiagente distribuído 
ou centralizado &  Ambos \\ \hline
  
  Multi-agent Planning by Plan Reuse (\cite{mapr})& 2013 & AAMAS & A1 & Distribuição de objetivos entre os agentes & 
Fraco  \\ \hline
      
    A Formal Analysis of Required Cooperation in Multi-Agent Planning (\cite{cooperation})& 2016 & ICAPS & A2 & Algoritmo para 
determinar se é preciso haver cooperação entre agentes para atingir objetivos & Ambos \\ \hline

    FMAP: Distributed cooperative multi-agent planning (\cite{fmap})& 2014 & \textit{Applied Intelligence} & B1 & 
Planejador independente de 
    domínio utilizando a técnica de refinamento de planos & Ambos \\ \hline
    
    MAPJA: Multi-agent planning with joint actions (\cite{mapja})& 2017 & Applied Intelligence & B1 & Planejador 
multiagente independente de 
  domínio e capaz de resolver ações conjuntas &  Ambos\\ \hline
    
    Transforming Multi-agent Planning Into Single-Agent Planning Using Best-Cost Strategy (\cite{bracis})& 2016 & BRACIS & 
B2 & Análise do ganho 
  com distribuição de objetivos & Fraco \\ \hline
  \end{tabular}
  \label{cap2:extracao}
  \end{table}
\end{landscape}

