\label{sma}

O primeiro passo para entender um Sistema Multiagente (SMA) é a caracterização do conceito de agente. Apesar de não existir um 
consenso universal, a Definição \ref{def:agente}, apresentada em \citet{wooldridge}, sintetiza os termos principais.

\begin{defn}
 Um \textbf{agente} é um sistema de computador que está situado em algum \textbf{ambiente} e que é capaz de executar 
\textbf{ações} autonomamente nesse ambiente para atingir seus objetivos.
\label{def:agente}
\end{defn}

Uma caracterização que naturalmente surge a partir dessa definição é a noção de agente inteligente, que deve apresentar as 
seguintes capacidades:

\begin{itemize}
 \item Reatividade - perceber o ambiente e reagir às mudanças ocorridas nele a fim de satisfazer seus objetivos;
 \item Proatividade - exibir um comportamento orientado a objetivos, tomando a iniciativa da realização de ações que conduzem aos 
objetivos;
 \item Sociabilidade - interagir com outros agentes.
\end{itemize}

A formalização do agente depende de dois conceitos: ambiente e ações. O ambiente é um conjunto finito de estados $E = \{e_0, e_1, 
\ldots,e_n\}$. Os agentes possuem uma coleção de ações disponíveis, $Ac = \{\alpha_0, \alpha_1, \ldots, \alpha_m\}$, que 
transformam o ambiente. 

Esses dois conceitos se relacionam da seguinte maneira: o ambiente inicialmente está em um estado $s_0$ e o agente escolhe uma 
ação $\alpha_j$ para executar a partir de $s_0$; como consequência dessa ação, o ambiente assumirá um novo estado e então o 
agente escolhe uma segunda ação; esse ciclo se repete até que uma condição de parada seja atingida. A partir dessa sequência de 
mudanças, o conceito de \textit{execução}, presente em \citet{wooldridge}, é apresentado na Definição \ref{def:run}.

\begin{defn}
 Uma execução $r$ de uma agente em um ambiente é uma sequência de estados do ambiente e ações intercaladas, conforme apresentado 
na Equação \ref{eq:run}.
 \label{def:run}
\end{defn}
 \begin{equation}
 \label{eq:run}
    r: e_0 \xrightarrow{\alpha_0} e_1 \xrightarrow{\alpha_1} \ldots e_{u-1} \xrightarrow{\alpha_u} e_u
 \end{equation} 


As possíveis execuções $r$ podem ser agrupadas formando os conjuntos:

\begin{itemize}
 \item $R$ - conjunto de todas sequências finitas de $r$, ou seja, $R =\{r_0, r_1, \ldots\}$;
 \item $R^{Ac}$ - subconjuntos de $R$ que acabam com uma ação;
 \item $R^E$ - subconjuntos de $R$ que acabam com um estado de ambiente.
\end{itemize}

As ações de um agente causam efeitos no ambiente, que por sua vez são representados por uma função de transformação de estado, 
conforme definida em \citet{wooldridge} e apresentada na Equação \ref{eq:tau}. Portanto, uma função $\tau$ mapeia uma execução 
$r$ em um conjunto de partes dos possíveis estados resultantes da execução da ação $\alpha$ que encerra $r$.

\begin{equation}
 \tau: R^{Ac} \rightarrow \wp(E)
 \label{eq:tau} 
\end{equation}

Formalmente, o ambiente é uma tupla $Env = <E, e_0, \tau>$, onde $E$ é um conjunto de estados de ambiente, $e_0 \in E$ é um 
estado inicial, e $\tau$ é a função de transformação (Equação \ref{eq:tau}).

O ambiente pode apresentar características importantes que o define, conforme \cite{wooldridge, russell}, tais como:

\begin{itemize}
 \item Acessível $\times$ Inacessível - um ambiente acessível, ou totalmente observável, é aquele no qual o agente pode obter 
informações completas, precisas e atuais sobre os estados do ambiente. Quando essa condição não é atendida, o 
ambiente é 
considerado inacessível ou parcialmente observável;
 \item Determinístico $\times$ Não-determinístico - se não há incerteza em relação às execuções dos efeitos das ações, o ambiente 
é dito determinístico;
 \item Estático $\times$ Dinâmico - ambientes nos quais as mudanças são causadas apenas por intervenções das ações dos agentes 
são classificados como estáticos. Quando essas atualizações podem ocorrer como consequência de eventos externos aos agentes, o 
ambiente é dinâmico;
 \item Discreto $\times$ Contínuo - um ambiente é discreto se há um número fixo e finito de ações e percepções sobre ele, caso 
contrário é denominado contínuo;
 \item Episódico $\times$ Sequencial - quando as ações realizadas em um estado (episódio) do ambiente não depende daquelas 
realizadas anteriormente, o ambiente é episódico. Quando a decisão atual afeta as decisões futuras, o ambiente é sequencial;
 \item Único agente $\times$ Múltiplos agentes - classificação de acordo com o número de agentes existentes no ambiente.
\end{itemize}


Finalmente, agentes podem ser definidos como funções que mapeiam execuções em ações, conforme a Equação \ref{eq:agente} 
apresentada em \citet{wooldridge}. 

\begin{equation}
 Ag: R^E \rightarrow Ac
 \label{eq:agente}
\end{equation}

Dessa forma, a relação entre agente e ambiente pode ser caracterizada pelo conjunto das possíveis execuções do agente no 
ambiente, representados por $R(Ag, Env)$.

Um SMA é um conjunto de entidades de software, denominadas agentes (Definição \ref{def:agente}), que são capazes de perceber e 
alterar o estado do ambiente (Definição \ref{def:run}), no qual estão inseridos, por meio de seus sensores e atuadores, 
respectivamente. Como sistemas 
inteligentes, os agentes apresentam níveis de autonomia baseados nos processos de raciocínio. A maneira pela qual esse processo é 
implementado serve para classificar os agentes.

Os agentes mais simples de se implementar são denominados \textit{reativos}, neste caso a escolha da ação é baseada apenas na sua 
percepção atual do ambiente, ignorando todo o histórico de eventos anteriores do ambiente. Também existem os agentes 
\textit{orientadas a modelo} quando mantém uma descrição de como o ambiente é modificado, seja por intervenção direta do agente 
ou por um terceiro. No cenário de 
planejamento multiagente, essa capacidade é útil quando é necessário lidar com uma observação parcial do ambiente, fato que não 
satisfaz uma das hipóteses assumidas no cenário de planejamento clássico (Item nº 2 da relação de hipóteses apresentada na Seção 
\ref{planejamento}, Página \pageref{classicalPlanning}). Outra classificação para agentes \textit{orientados a 
objetivos}, mostrados na Figura \ref{img:goalAgent} apresentada e traduzida \cite{russell}, realizam a seleção das ações 
baseadas no 
estado atual e nos objetivos, tornando-as integráveis às 
técnicas de planejamento automatizado. 
Entretanto, a noção de objetivos não garante que as ações escolhidas impliquem em um comportamento eficiente. Dessa forma, os 
agentes \textit{baseados em utilidade} baseiam-se principalmente em fatores como custo e duração de execução para determinar a 
melhor opção (a hipótese de custo geralmente é desconsiderada no planejamento automatizado clássico). Por fim, um agente com 
\textit{aprendizagem} é capaz de estender seu conhecimento inicial com o intuito de tornar mais eficiente e racional seu processo 
deliberativo.

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.5]{images/agente.png}
 \caption{Agente orientado a objetivos.}
 \label{img:goalAgent}
\end{figure}

Outro aspecto importante é a capacidade de comunicação entre os agentes, fato esse que permite que eles se relacionem e coordenem suas 
ações em um ambiente compartilhado seguindo a taxonomia apresentada na Figura \ref{img:taxonomia} e 
traduzida de \cite{weiss2013modern}. Devido 
a questões de eficiência e 
robustez e por ser essencialmente distribuído, um SMA deve preferencialmente executar o controle das atividades de forma descentralizada, 
caracterizando essa coordenação como o processo pelo qual decisões individuais resultam em boas decisões conjuntas para o grupo 
\cite{vlassis}.


Por um lado, se essa coordenação envolver agentes antagônicos caracterizando um cenário de competição, um protocolo de negociação 
é necessário para definir o comportamento adotado pelos agentes. Por outro lado, se existir cumplicidade entre eles em um 
cenário de cooperação, um protocolo de planejamento deve ser empregado, seja de forma centralizada ou distribuída. A principal 
motivação para existência desse protocolo é a necessidade da determinação de quais objetivos cada agente tentará 
satisfazer ou de quais tarefas realizarão, evitando contenção e situações de conflitos no uso dos recursos.

\begin{figure}
 \centering
 \includegraphics[scale=0.2]{images/taxonomia.png}
 \caption{Taxonomia de coordenação.}
 \label{img:taxonomia}
\end{figure}

Os protocolos de interação entre agentes incluem diferentes abordagens de coordenação, cooperação, \textit{contract net}, 
\textit{blackboard systems}, negociação, manutenção de crença e mecanismos de mercado \cite{weiss2013modern,wooldridge}. Esses 
protocolos regem a forma de 
conversação entre os agentes, incluindo as trocas de mensagens necessárias para execução de tarefas. Dentre os possíveis 
protocolos, o \textit{contract net} é um dos protocolos mais utilizados para negociação entre agentes, resultando em um meio eficiente 
de distribuição de tarefas \cite{weiss2013modern}. As atividades que definem esse protocolo formam um 
conjunto de quatro etapas sequenciais:

\begin{enumerate}
 \item o agente reconhece que existe um problema e então inicia a comunicação com 
os demais elementos; 

 \item esse agente contratante realiza um anúncio da tarefa que deseja resolver de acordo com o nível de conhecimento que tem 
dos 
demais agentes. Quando não possui nenhuma informação sobre suas capacidades, o agente é obrigado a enviar mensagens a todos os 
nós presentes na rede 
(\textit{broadcast}). Entretanto, caso o contratante possua algum conhecimento sobre quais são os possíveis candidatos, ele pode 
realizar um 
comunicação limitada (\textit{multicast}). Por fim, se o nó apropriado para a tarefa for conhecido, a comunicação pode ser feita diretamente 
(\textit{point-to-point});

\item os agentes que receberam o anúncio realizam uma auto-análise para verificar se possuem os recursos necessários 
para realizar a tarefa e em caso positivo, cada entidade responde com o custo referente ao pedido;

\item o contratante decide qual agente contratará de acordo com sua estratégia de seleção, por exemplo de 
minimização de custo, e então enviará uma mensagem ao agente a ser contratado encerrando a execução do protocolo.

\end{enumerate}

Visando implementar esse processo deliberativo, os agentes podem empregar técnicas de planejamento automatizado, caracterizando a 
área de 
pesquisa de Planejamento Multiagente (\textit{Multi-Agent Planning - MAP}).