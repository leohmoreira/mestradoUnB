Um método de coordenação visa facilitar a etapa de planejamento, mas a tentativa de construir uma abordagem independente de 
domínio esbarra nos problemas de escalabilidade e controle. Em domínios fortemente acoplados os agentes 
explicitamente necessitam cooperar para atingir os objetivos, uma vez que uma simples entidade não possui todas as capacidades 
requeridas para executar a tarefa. Por outro lado, em domínios fracamente acoplados, a coordenação pode ser flexibilizada uma vez 
que um único agente pode satisfazer sozinho todos ou um subconjunto dos objetivos.

Uma contribuição para a área de planejamento multiagente identificada na revisão de literatura (vide Capítulo 
\ref{cap:trabalhosCorrelatos}) é a combinação das características de verificação e transformação do problema. As análises da 
possibilidade de atingir os objetivos e da necessidade de cooperação entre os agentes, quando realizadas em uma fase 
anterior ao processo de exploração do espaço de busca, pode apresentar uma diminuição do tempo de planejamento.

Uma vez que as condições necessárias para a solução do problema tenham sido identificadas e respeitem as restrições impostas, a 
transformação do MAP depende da seleção dos agentes e da alocação dos objetivos. Considerando especialmente a atividade de 
alocação, sua utilização permite explorar técnicas de execução em paralelo paralelismo, instanciando processos mais simples de 
planejamento para cada agente, considerando o fato que as plataformas computacionais possuem recursos limitados. Uma vez que as 
entidades devem analisar um problema menos complexo, com um conjunto menor de ações, restrições e efeitos, o tempo gasto para 
calcular um plano final ou parcial tende a ser menor. Em outras palavras, sistemas multiagentes fornecem a 
infraestrutura necessária para execução paralela. 

Portanto, a efetividade de um modelo de planejamento multiagente depende do desenvolvimento de métodos capazes de:

\begin{itemize}
 \item distribuir eficientemente o trabalho entre as entidades disponíveis; 
 \item garantir as condições de cooperação quando necessárias;
 \item coordenar as atividades utilizando uma quantidade de mensagens que permita a escalabilidade do modelo.
\end{itemize}

O modelo proposto visa tanto preencher as lacunas identificadas nos trabalhos correlatos quanto configurar uma importante 
contribuição e avanço para a área de planejamento multiagente. A Tabela \ref{tab:caracModelo}, apresenta a comparação das 
características do modelo com os trabalhos identificados. A flexibilização da entrada para permitir o uso de diversas 
linguagens para definição de domínio e problema não é tratada nesse trabalho para ajustar o escopo ao tempo disponível.

\begin{table}[htbp]
 \centering
 \caption{Contribuição e características do modelo proposto.}
 \begin{footnotesize}
  
  \begin{tabular}{|l|c|c|c|c|c|c|c|}
    \hline
    \multirow{2}{*}{Trabalho} & \multicolumn{7}{c|}{Características} \\ \cline{2-8}
     & C1 & C2 & C3 & C4 & C5 & C6 & C7 \\ \hline
    \citet{cooperation} & - & - & \checkmark & \checkmark & - & Centralizada & Ignorada \\ \hline
    \citet{fmap} & - & - & - & - & - & Distribuída & Preservada \\ \hline
    \citet{mapja} & - & - & - & \checkmark & \checkmark & Centralizada & Ignorada \\ \hline
    \citet{madla} & \checkmark & - & - & - & \checkmark & Centralizada & Preservada\\ \hline
    \citet{mapr} & - & - & - & - & \checkmark & Distribuída& Preservada\\ \hline
    \citet{bracis} & - & - & - & \checkmark & \checkmark & Centralizada & Ignorada\\ \hline
    \rowcolor[rgb]{0.8,0.8,0.8} Modelo Proposto & - & \checkmark & \checkmark & \checkmark & \checkmark & Centralizada & 
Ignorada \\ \hline
  \end{tabular}
 \end{footnotesize}
 \label{tab:caracModelo}
\end{table}

\section{Visão Geral}

No modelo proposto, os agentes são caracterizados por três componentes principais: 
\begin{itemize}
 \item arquivo que descreve o domínio;
 \item arquivo que descreve o problema;
 \item planejador automatizado.
\end{itemize}

A descrição dos dois primeiros componentes segue o padrão \textit{Domain Definition Language (PDDL)}. No arquivo de domínio, as 
capacidades do agente são listadas como ações compostas por conjuntos de pré-condições que devem ser satisfeitas para permitir 
a execução e de efeitos, isto é, mudanças no ambiente. Considerando à arquitetura de uma agente (vide Figura 
\ref{img:goalAgent}), esses conjuntos representam os sensores e atuadores, respectivamente. Uma vez que as ações tratam 
predicados e variáveis de estado que definem a condição do ambiente, eles também ser listado no arquivo de domínio. 

O arquivo de problema expressa o estado inicial, isto é, a percepção que o agente tem do ambiente em um primeiro momento. Os 
objetivos e os objetos sobre os quais as ações são executadas também são descritos nesse arquivo. Não há nenhuma anotação 
explícita sobre os objetivos, portanto eles não são classificados como individuais ou globais. 
% Analogamente, não há nenhum tipo de preocupação em relação à 
% privacidade das informações uma vez que é assumido um cenário de completa cooperação entre os agentes sem restrição de troca de 
% dados.

Com relação ao terceiro componente, o uso de planejador automatizado permite ao agente deliberar sobre suas ações, garantindo-lhe 
autonomia e a capacidade de determinar uma sequência de ações que alcancem os objetivos a partir do estado inicial. 

A proposta segue algumas premissas do planejamento clássico. O conjunto de estados do ambiente é finito, totalmente observável e 
estático. Os efeitos das ações são determinísticos e instantâneos. O processo de planejamento é realizado \textit{offline}. 

Embora a linguagem PDDL suporte a disjunção de objetivos, o modelo proposto permite apenas a conjunção deles. Portanto, a 
ausência de anotações que caracterizem a condição dos objetivos (individual ou global) não causa nenhum prejuízo pois todos eles 
devem ser satisfeitos simultaneamente por um único estado. 

Como partes de um sistema maior, os agentes devem trabalhar de maneira cooperativa sob um coordenação central que garanta uma 
utilização eficiente dos recursos. Esse papel é desempenhado pelo coordenador que é responsável por selecionar os 
agentes capacitados e então alocar os objetivos a eles. Sempre que todas entidades tenham condições de atingir os objetivos de 
maneira isolada e sem necessidade de interação, o que configura um problema fracamente acoplado, os procedimentos de seleção e 
alocação são necessário e importantes para evitar que dois ou mais agentes trabalhem simultaneamente em prol do mesmo objetivo, a 
não ser no caso de ações conjuntas. Quando nenhum agente tiver as condições necessárias para satisfazer os objetivos 
individualmente, o coordenador efetua o planejamento de maneira centralizada, dado que possui conhecimento sobre todas capacidades 
e restrições dos agentes.

Nesse ponto, outra premissa do modelo é que a questão de privacidade não é foco do trabalho posto que ela é considerada relevante 
em domínios nos quais competição é um aspecto importante. Portanto, um cenário de completa cooperação entre os agentes é 
assumido sem qualquer tipo de restrição de troca de informação.

A arquitetura do modelo proposto e a interação entre os agentes e o coordenador estão apresentadas na Figura 
\ref{fig:arquiteturaModelo}. Primeiramente, o coordenador anuncia os objetivos para todas as entidades que por sua vez avaliam 
suas capacidades. Em seguida, cada agente individualmente avalia a insolvabilidade dos objetivos. Se ele puder satisfazer todas 
metas apenas com suas capacidades, não há dependências e portanto é apenas necessário alocar os objetivos ao agente. Portanto, a 
situação configura um problema fracamente acoplado. Em situações fortemente acopladas nas quais os agentes precisam interagir, 
eles anunciam suas capacidades ao coordenador que então calcula o plano. 
% Existe também um terceiro caos possível no qual um ou 
% mais agentes podem satisfazer parcialmente o conjunto de objetivos, ou seja, um subconjunto deles. Neste caso, o coordenador 
% inicia o processo de alocação, definindo quais metas cada agente deve atingir e também planeja de forma centralizada a fim de 
% satisfazer os objetivos restantes.

\begin{figure}
 \centering
%  \includegraphics[width=0.9\textwidth]{images/arquiteturaModelo.png}
\includegraphics[width=0.9\textwidth]{images/diagramaDeInteracao.png}
 \caption{Interação entre agentes.}
 \label{fig:diagramaDeInteracao}
\end{figure}

\begin{figure}
 \centering
%  \includegraphics[width=0.9\textwidth]{images/arquiteturaModelo.png}
\includegraphics[width=0.9\textwidth]{images/modeloConceitual.png}
 \caption{Modelo conceitual.}
 \label{fig:modeloConceitual}
\end{figure}

\section{Componentes do modelo}

A Figura \ref{fig:arquiteturaModelo} destaca também destaca os componentes presentes na arquitetura do modelo e organizados 
em duas fases. A primeira delas caracteriza uma etapa pré-planejamento na qual os procedimentos de tradução, teste de 
insolvabilidade, análise de cooperação, anúncio, teste global de insolvabilidade, seleção de agentes e alocação de objetivos são 
executados como preparação para a fase seguinte. Na segunda etapa, cada agente utiliza seu planejador automatizado para 
determinar a 
sequência de ações necessárias para atingir os objetivos, caracterizando assim a fase de planejamento. Esses procedimentos são 
detalhados a seguir.

\subsection{Tradução}

No procedimento de tradução, os arquivos PDDL são processados (\textit{parsed}) e todos possíveis predicados são calculados 
considerando as definições de domínio e problema. Cabe ressaltar que essa característica aprimora os \textit{parsers} comuns. 
Todos predicados são gerados usando o produto cartesiano dos domínios de cada variável definida.

Apesar de ser facilmente codificado, essa estratégia implica em alguns efeitos colaterais porém nenhum deles prejudica a 
corretude do modelo. O cálculo do produto cartesiano gera todos predicados possíveis, porém alguns deles podem não atingidos 
dependendo das condições de contorno do problema, tais como as pré-condições das ações. Esses predicados inalcançáveis não causam 
erros ou soluções incorretas, entretanto, eles demandam uma avaliação extra, afetando o tempo de resposta. O modelo proposto 
contorna essa questão através do uso de técnicas de execução paralela.

\subsection{Teste de insolvabilidade}

O centro da fase de pré-planejamento é representado pelos procedimentos de teste de insolvabilidade que são executados 
separadamente nos agentes e no coordenador (teste global de insolvabilidade). Inicialmente, cada agente avalia sua capacidade de 
atingir individualmente os objetivos. Nesse sentido, a heurística \textit{delete-relaxation} é aplicada. 

Basicamente, essa técnica explora o conjunto de ações disponíveis de um único agente, adicionando novos efeitos e nunca removendo 
os antigos. Ela retorna um valor que representa o custo para atingir um estado a partir de outro. No modelo, o custo é 
considerado como o número de ações necessárias para satisfazer os objetivos, dado que é desejável um plano com o menor tamanho 
possível. Entretanto, outras representações de custo tais como tempo ou consumo de recursos podem ser exploradas. Um valor 
infinito significa que o agente não pode atingir o estado desejável sozinho, ou seja, apenas com suas ações. Qualquer outro valor 
indica um valor heurístico do esforço do agente. Apesar do fato dessa técnica transformar o original em uma instância mais 
simples (relaxada), seu uso é capaz de fornecer informações importantes sobre a insolvabilidade do problema de planejamento. Em 
outras palavras, se um objetivo for inalcançável sob condições relaxadas, certamente ele também será em um contexto mais restrito.

Portanto, o teste de insolvabilidade é capaz de verificar tanto se todas capacidades e objetos necessários estão disponíveis 
quanto a existência uma sequência de ações que atinge um estado que satisfaz todos objetivos. Ao final da verificação individual, 
cada agente anuncia ao coordenador não apenas a possibilidade de atingir as metas, parcial ou totalmente, mas também as 
restrições referentes à incapacidade de satisfazê-las.

Nesse instante, o coordenador executa o teste global de insolvabilidade. Ele usa a mesma heurística de 
\textit{delete-relaxation}, porém, ele avalia o conjunto de ações de todos agentes. Esse procedimento é importante visto que os 
agentes podem ser incapazes de satisfazer individualmente os objetivos, mas se cooperarem, eles podem atingir as metas. Portanto, 
o coordenador analisa todas capacidades disponíveis a fim de verificar a possibilidade de encontrar uma solução para o problema. 

O algoritmo utilizado para calcular o valor heurístico em ambos os testes é apresentado na Figura \ref{alg:algoritmoHFF}.

\begin{figure}
 \centering
 \includegraphics[scale=0.5]{images/algoritmoHFF.png}
 \caption{Algoritmo para heurística \textit{delete-relaxation}\cite{ghallab2016}.}
 \label{alg:algoritmoHFF}
\end{figure}

Um ponto importante a ser destacado é que o uso dessa heurística é verificar se o problema não é impossível de ser 
solucionado. Nesse sentido, existe uma ligeira diferença entre ser possível e não ser impossível. O primeiro caso só é 
concretizado quando um plano é apresentado e isso é apenas realizado ao final da fase de planejamento. O foco do teste de 
insolvabilidade é, portanto, verificar se as condições necessárias para a solução estão disponíveis, sejam elas a presença de 
todas capacidades e objetos dos quais os objetivos dependem. 

\subsection{Análise de cooperação}

\textit{Nessa seção vou descrever os detalhes apresentado em \citet{cooperation} e como vou explorar a técnica no modelo.}

\subsection{Seleção de agentes}

Uma vez que os objetivos foram considerados não impossíveis, o procedimento de seleção é iniciado. O coordenador avalia as 
capacidades e restrições recebidas de cada agente e decide quais recursos são necessários para satisfazer os objetivos.

O modelo pode executar a seleção de duas maneiras distintas. Ele pode identificar o menor número de agentes que serão usados na 
fase de planejamento, portanto, mesmo que todas entidades sejam capazes de atingir as metas, apenas uma delas será empregada. 
Nesse sentido, a economia na alocação dos recursos é o propósito principal e uma seleção gulosa (\textit{greedy}) é usada.

Quando um balanceamento de carga é importante, o modelo explora a estratégia de \textit{round-robin} a fim de selecionar tantos 
agentes quanto forem necessários. Logo, o coordenador não foca em um uso eficiente mas em respostas mais rápidas, considerando 
os tempos gastos tanto na fase de planejamento quanto de execução do plano.

\subsection{Alocação de objetivos}

Após a seleção dos agentes, o coordenador inicia o procedimento de alocação de objetivos considerando as restrições de cada 
agente. 

\textit{Apresentar os algoritmos}


\subsection{Execução paralela}

Uma característica importante do modelo é que os testes de insolvabilidade são independentes entre os agentes, ou seja, cada 
um agente não depende de outro para verificar quais objetivos pode satisfazer. Esse fato permite que essas análises sejam 
realizadas paralelamente sem nenhum prejuízo quanto a precisão dos resultados.

Para tanto, cada agente é instanciado como um processo separado permitindo que todos recursos do ambiente computacional 
(processadores e \textit{threads}) sejam explorados na plenitude. O aprimoramento que essa decisão traz é que o tempo gasto nessa 
fase de pré-planejamento pode ser reduzido, simplesmente aplicando técnicas de execução paralela.