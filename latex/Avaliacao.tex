A fim de avaliar a eficácia e eficiência do modelo proposto (LCMAP), experimentos foram realizados a partir 
de casos retirados da \textit{International Planning Competition} (IPC) \cite{jair20}, por esta ser uma fonte largamente 
explorada nos trabalhos 
correlatos. Entre as opções disponíveis, seis domínios foram escolhidos, sendo três fracamente e outros três fortemente 
acoplados. 

Os resultados obtidos foram comparados com dois outros modelos de planejamento multiagente: FMAP\footnote{Disponível em: 
\url{http://users.dsic.upv.es/grupos/grps/tools/map/fmap.html}} \cite{fmap} e MADLA\footnote{Disponível em: 
\url{https://github.com/stolba/MADLAPlanner/wiki/MADLA-Planner}} \cite{madla}. As razões que motivaram a escolha desses dois 
sistemas foram:
\begin{itemize}
 \item ambos disponibilizam implementações executáveis que permitem a realização de testes;
 \item no caso do FMAP, o trabalho original compara seu desempenho com outras duas ferramentas: MAP-POP \cite{mappop} e MAPR 
\cite{mapr}. Sendo assim, a escolha desse modelo permite a comparação, de forma indireta, com esses dois outros trabalhos.
\end{itemize}

As configurações de planejamento, ou seja, domínio e problema, foram semelhantes aos exemplos definidos nos casos de 
testes do FMAP \cite{fmap}, a menos de pequenas diferenças na estrutura da linguagem PDDL que foi estendida (pelo FMAP) para 
comportar 
marcações sobre os objetivos e compartilhamento de informações.

Os domínios escolhidos para testes do IPC \cite{jair20} são:

\begin{itemize}
 \item \textit{Satellite} - cada agente simboliza um satélite que é 
individualizado por seus atributos referentes à posição, orientação (direção) e instrumentos disponíveis. Apesar do fato de 
possuírem qualidades diferentes, a transformação do problema original é favorecida pelo fato dos agentes não necessitarem de 
cooperação para executar suas ações. O problema consiste em escalonar observações dos satélites que incluem a coleta e o 
armazenamento de dados usando os diferentes instrumentos para observar uma coleção de alvos;

% Os objetivos nesse domínio configuram capturas de imagens de determinados pontos 
% sob certas condições (térmica e espectral) utilizando equipamentos capazes de atendê-las. Assim, mesmo sendo fracamente 
% acoplados, cada agente apresentará um conjunto específico de objetivos alcançáveis dependentes de suas restrições de posição e 
% instrumentos;

 \item \textit{Rovers} - motivado pelas missões de exploração de Marte. O objetivo é usar um conjunto de 
robôs (agentes) para percorrer pontos do planeta realizando uma variedade de operações de coleta de amostras e transmissão de 
dados para a base. O problema inclui restrições de visibilidade dessa base a partir de várias posições e de habilidade de cada 
agente percorrer caminhos entre pares de pontos. Os robôs são diferenciados pelo conjunto de equipamentos que possuem e utilizam 
para executar as tarefas. Não há necessidade de cooperação durante a execução e a única interação possível é quando uma amostra é 
coletada e portanto torna-se indisponível para os demais agentes;
%  Da mesma forma que o caso \textit{satellite}, esse domínio explora a decomposição do conjunto 
% de objetivos e a delegação de seus itens entre os agentes.  \textit{depósitos} e \textit{caminhões},
% Cada robô (agente) deve percorrer uma região para fotografá-la 
% e para coletar amostras do solo e de rochas e são diferenciados por possuírem equipamentos para realizar tais tarefas. Os agentes 
% não necessitam de cooperação e única interação possível é quando uma amostra é coletado e portanto torna-se indisponível para os 
% demais robôs;

 \item \textit{Zeno-travel} - relacionado a uma questão de transporte na qual as pessoas devem embarcar em 
aviões, viajar entre localidades e então desembarcar. Os aviões consumem combustíveis em diferentes taxas de acordo com a 
velocidade do deslocamento;

 \item \textit{Depots} - composto por dois tipos de agentes, \textit{depósitos} e \textit{caminhões}, que devem 
cooperar a fim de satisfazer os objetivos. Esse é o caso mais complexo dos testes e configura um domínio fortemente acoplado com 
muitas dependências entre os agentes \cite{fmap};

 \item \textit{Elevators} - os agentes nesse domínio são elevadores que variam na velocidade, podendo ser rápidos ou lentos, e 
nos 
andares que podem acessar. Essa última característica define a necessidade de cooperação para que alguns objetivos sejam 
satisfeitos. Por exemplo, um elevador pode não ser capaz de transportar um passageiro para um certo andar, então ele irá até um 
andar intermediário para que o passageiro possa entrar em outro elevador que acessa o destino final \cite{fmap};

 \item \textit{Logistics} - os agentes nesse domínio são \textit{aviões} e \textit{caminhões}. A entrega de alguns pacotes 
envolvem a cooperação de elementos dos dois tipos, dado que a viagem entre cidades só pode ser realizada por aviões e 
o deslocamento dentro da mesma cidade são de responsabilidade dos caminhões \cite{fmap}.

\end{itemize}

Todos os experimentos foram realizados em um único computador com um processador \textit{dual-core Intel Core i5-2410M} com 
quatro \textit{threads} e 6 GB RAM. O ambiente operacional utilizado foi o \textit{Linux Mint 18.1 Serena 64-bit}.

Para os domínios fracamente acoplados foram utilizados de um a cinco agentes. Para os fortemente acoplados, o limites variam entre 
três e oito de acordo com o domínio. Em relação ao planejador automatizado, todos agentes utilizaram o \textit{Fast-Forward (FF)} 
\cite{ff1}. Por disponibilizar duas estratégias diferentes de planejamento, as referências aos resultados do LCMAP tiveram 
acrescidos rótulos para identificar se foi almejado o balanceamento de carga (LCMAP-BC) ou a economia (LCMAP-E) de agentes.

Nas Tabelas \ref{tab:satellite}, \ref{tab:rovers}, \ref{tab:zenotravel}, \ref{tab:depots}, \ref{tab:elevators} e 
\ref{tab:logistics} são apresentados os resultados comparativos entre os modelos usados. As métricas utilizadas foram:

\begin{itemize}
 \item \textit{Tempo} - intervalo gasto pela aplicação até a apresentação do plano. No caso do LCMAP e MADLA, que são 
executados por linhas de comando, o tempo compreendido entre o início e o término do processo foi medido através do uso do 
comando \textit{time}\footnote{\url{https://linux.die.net/man/1/time}}. No caso do FMAP, o processo não é encerrado com o término 
do planejamento e por isso tal técnica não pode ser aplicada. Entretanto, o tempo gasto por cada agente é apresentado assim que o 
plano é encontrado, e portanto o maior desses valores foi assumido;

\item \textit{Número de ações}: Soma dos tamanhos das sequências calculadas de maneira independente ou do plano centralizado;

\item \textit{Agentes empregados}: Quantidade de agentes utilizados durante o planejamento independente ou empregados 
na execução do plano computado de coordenador.
\end{itemize}

A primeira avaliação feita a partir dos resultados é baseada na métrica de tempo. Para aprimorar a visualização dos valores, as 
Figuras \ref{img:resultadoSatellite}, \ref{img:resultadoRovers}, 
\ref{img:resultadoZenotravel}, \ref{img:resultadoDepots}, \ref{img:resultadoElevators} e 
\ref{img:resultadoLogistics} foram construídas com o intuito de facilitar a comparação do desempenho. 

É possível verificar que a variação de valores é bem significativa, podendo atingir um aumento de $1000 \times$ entre as 
simulações de um e cinco agentes (nos domínios \textit{satellite} e \textit{zeno-travel}); por esse motivo, os gráficos são 
apresentados com escala logarítmica no Eixo Y. 

Na quase totalidade dos resultados, o tempo gasto nas execuções caracterizou-se 
como uma função do número de agentes e objetivos com a propriedade de ser monótona e crescente. A única exceção é feita à 
configuração com quatro agentes no domínio \textit{elevators} utilizando FMAP, caso no qual a saída apresentou uma diminuição de 
aproximadamente $50\%$ quando comparada com a anterior. Entretanto, fica claro que qualquer solução é sensivelmente impactada 
tanto pelo aumento do número de agentes envolvidos quanto pela quantidade de objetivos. Cabe ressaltar que apesar das 
configurações criarem uma correlação positiva entre essas variáveis, os experimentos do domínio \textit{elevators} mostraram que 
mesmo mantendo constante o conjunto de metas, a adição de novos membros no sistema implicou em uma resposta mais demorada. De uma 
forma indireta, essa conclusão mostra que a hipótese assumida na fase de atribuição dos objetivos é válida e portanto justifica a 
transformação do problema original.

Da análise dos domínios fracamente acoplados, \textit{satellite} (Figura \ref{img:resultadoSatellite}), \textit{rovers} (Figura 
\ref{img:resultadoRovers})e \textit{zeno-travel} (Figura \ref{img:resultadoZenotravel}), os resultados obtidos 
a partir do uso do modelo LCMAP, independente da estratégia escolhida, foram melhores, isto é, menores em $71,43\%$ (10 em 14) 
dos testes. Esse comportamento ilustra que explorar a ausência da necessidade de cooperação entre os agentes, tanto na fase de 
planejamento quanto na de execução, é uma alternativa de mitigação dos efeitos do aumento do número de membros no sistema. Na 
comparação restrita ao FMAP, enquanto essa solução utiliza uma forte interação entre os planejadores a cada iteração do processo 
do refinamento dos planos, o LCMAP apresentou um desempenho melhor por explorar o desacoplamento total dos agentes. Nos quatro 
testes nos quais o LCMAP apresentou desempenho inferior ao FMAP (\textit{rovers} com um agente e \textit{zeno-travel} até três 
agentes), a razão foi o custo (tempo) das execuções das fases de verificação e transformação do problema, atividades que não 
estão presentes no FMAP. Os resultados apresentados pelo MADLA foram os piores tanto em valores absolutos quanto na 
abrangência, além disso foi incapaz de resolver cinco problemas. Ainda considerando esse quesito de número de instâncias 
resolvidas, o LCMAP foi também superior, resolvendo $100\%$ dos casos enquanto o FMAP apresentou um insucesso.

Da análise dos domínios fortemente acoplados, \textit{depots} (Figura \ref{img:resultadoDepots}), \textit{elevators} (Figura 
\ref{img:resultadoElevators}) e \textit{logistics} (Figura \ref{img:resultadoLogistics}), o LCMAP apresentou 
resultados melhores em $75\%$ (6 em 8) dos testes. Nesses casos, apesar das fases de pré-planejamento, o ganho foi devido o papel 
centralizador do coordenador que computou os planos sem a necessidade de uma forte interação com os demais agentes. Nas 
configurações nas quais teve um desempenho pior (\textit{elevators} com quatro e cinco agentes), o LCMAP foi no máximo $2 \times$ 
mais lento enquanto que nas vezes que superou os concorrentes chegou a ser aproximadamente $100 \times$ mais rápido 
(\textit{logistics} com 5 agentes).

Considerando os dois tipos de domínios, o modelo proposto foi superior em $72,72\%$ (16 em 22) dos experimentos e apresentou 
resultados muito próximos, independente da estratégia adotada na atribuição dos objetivos, isto é, balanceamento de carga ou 
economia de recursos.

Em relação ao número de agentes empregados, o LCMAP conseguiu apresentar soluções mais econômicas em $36,36\%$ (8 em 22) das 
configurações. Em uma análise desatenciosa esse desempenho pode parecer ruim porém considerando todas condições de contorno, esse 
valor pode ser muito melhor. Na realidade, a quantidade de casos que possibilitam o uso de um número menor de agentes deve 
desconsiderar aqueles com apenas um agente e também aqueles já definidos com o conjunto mínimo\footnote{A configuração 
\textit{elevators} com cinco agentes pode ser resolvida com quatro agentes.} (\textit{depots} e \textit{logistics}). Dessa forma, 
excluindo três e sete ocorrências das respectivas condições, o total de configurações passíveis de economia é na verdade doze. 
Nessas condições, o desempenho do LCMAP sobe para $66,67\%$ (8 em 12) e $87,5\%$ dos casos de seleções mais conservadoras 
ocorreram nos domínios fracamente acoplados, fato que já era esperado pela falta de necessidade de cooperação. Essa análise 
destaca outra vantagem do modelo proposto, que além de ser mais rápido é capaz de ser mais eficiente na alocação dos recursos do 
que os outros modelos comparados. A estratégia de balanceamento de carga do LCMAP não foi discutida porque, por definição, 
utiliza o maior número possível de agentes para que cada um receba o menor conjunto possível de objetivos.

Considerando o tamanho do plano final, em nove experimentos o LCMAP conseguiu encontrar soluções que apresentaram um número 
menor de ações.  Essa quantidade final pode ser igual ao plano centralizado ou resultado da soma dos tamanhos do soluções 
computadas de maneira independente. Apesar da diferença em relação ao segundo plano menor ter variado apenas entre uma e duas 
ações, essa característica ainda sim deve ser considerada como uma vantagem do modelo proposto.

Por fim, uma ressalva que merece destaque é o fato do LCMAP buscar sempre a transformação do problema, mesmo que a classificação 
do tipo de domínio induza à necessidade de cooperação, normalmente esperada nos casos fortemente acoplados. No experimento do 
domínio \textit{elevators} com quatro agentes, as condições definidas foram capazes de permitir que o problema fosse transformado 
em instâncias menores. Esse fato reforça a afirmação que muitos problemas de planejamento multiagente são passíveis de soluções 
individuais sob determinadas condições \cite{cooperation}.

\input{resultadoSatellite}

\input{resultadoRovers}

\input{resultadoZenotravel}

\input{resultadoDepots}

\input{resultadoElevators}

\input{resultadoLogistics}

\section{Escalabilidade}

Dois experimentos adicionais foram preparados para analisar a escalabilidade do LCMAP. Os domínios escolhidos foram 
\textit{rovers} e \textit{logistics} para avaliar os tipos de problemas fraca e fortemente acoplados, respectivamente. As razões 
que motivaram essas escolhas foram:
\begin{itemize}
 \item \textit{rovers} consiste em um conjunto de robôs que podem ser diferenciados por suas capacidades. Sendo assim, novos 
elementos podem ser criados a partir da configuração com cinco agente experimentada (Tabela \ref{tab:rovers} e Figura 
\ref{img:resultadoRovers}) variando apenas esse conjunto de capacidades. Como os objetivos desse domínio dependem basicamente de 
três operações, fotografar, coletar amostras de rochas e do solo, para cada robô (\textit{rover0}, $\ldots$, \textit{rover4}) 
são criados elementos idênticos em relação à posição inicial porém capazes de realizar uma, duas ou três das operações 
necessárias;
 \item \textit{logistics} permite a especialização dos agentes a partir de suas restrições de 
deslocamentos. A configuração avaliada no experimento com cinco agentes (Tabela \ref{tab:logistics} e Figura 
\ref{img:resultadoLogistics}) consiste em quatro cidades, cada uma com seu aeroporto, nas quais quatro caminhões e um avião 
cooperam para satisfazer os objetivos. Então, novos agentes podem ser instanciados e diferenciados pela cidade qual estão 
presentes. O aumento do número de elementos do sistema consiste nas inserções de novos agentes dos dois tipos com a condição de 
até quatro aviões (um por aeroporto) e até dezesseis caminhões (quatro por cidade);
\item ambos domínios apresentaram elevados valores de tempo para a configuração com cinco agentes, portanto configuram bons 
pontos de partida para a análise de escalabilidade;
\end{itemize}

A escolha da configuração com cinco agentes como ponto inicial dessa análise é devido ao fato do conjunto de 
objetivos ser plenamente satisfeito sob tais condições. Portanto, os agentes adicionados apenas contribuem como novas opções de 
escolha pelo coordenador. Cabe ressaltar que o número de objetivos foi mantido constante dado que o foco era avaliar o impacto do 
aumento do sistema. Por fim, para essa análise não serão realizados experimentos com os outros dois modelos dado que o LCMAP 
demonstrou resultados melhores em relação tanto ao tempo gasto quanto à abrangência.

Para analisar a escalabilidade do modelo uma nova métrica, descrita na Equação \ref{eq:taxa}, foi criada para descrever o impacto 
do crescimento do sistema no tempo de processamento. 

\begin{equation}
 aumento_i = \frac{\mbox{Tempo de processamento para $q_i$ agentes}} {\mbox{Tempo de processamento para $q_0$ agentes}},
 \mbox{com }q = \{5, 6 \ldots, 20\}
 \label{eq:taxa}
\end{equation}

A Tabela \ref{tab:escalabilidadeRovers} e a Figura \ref{img:escalabilidadeRovers} ilustram os tempos de planejamento gastos pelo 
LCMAP e as taxas de aumento utilizando as duas estratégias de atribuição disponíveis. A avaliação desses resultados obtidos no 
contexto do domínio \textit{rovers} mostrou que o LCMAP possui uma boa escalabilidade dado que em $84,37\%$ dos casos (27 de 32), 
o aumento de tempo (Equação \ref{eq:taxa}) foi menor que a taxa de crescimento do número de agentes. Essa propriedade é devido à 
transformação do problema original em instâncias menores, o que garante aos agentes independência entre si durante o 
planejamento. Cabe ressaltar que apesar dessa delegação de responsabilidades, um aumento no tempo total era esperado pois o 
ambiente de testes comporta apenas quatro processos simultâneos (quatro \textit{threads} de execução) e o total de agentes 
instanciados chegou a ser $5\times$ maior que essa capacidade. 

Entretanto, o modelo apresentou um comportamento quase constante no intervalo de $[5;10]$, o que provavelmente 
seria conservado nas demais configurações, caso a capacidade de execuções paralelas do computador fosse maior que a utilizada. Em 
relação aos outros modelos, um fato que merece destaque é que o tempo gasto pelo LCMAP para resolver o problema com vinte agentes 
é $3\times$ menor que aquele dispendido pelo FMAP para resolver um problema com apenas cinco elementos.

Em relação ao domínio fortemente acoplado, \textit{logistics}, apesar da escalabilidade apresentada não ser comparável ao caso 
anterior, os resultados ilustrados na Tabela \ref{tab:escalabilidadeLogistics} e na Figura \ref{img:escalabilidadeLogistics} 
foram úteis para apontar uma oportunidade de melhoria do modelo. Quando a necessidade de cooperação é percebida ao final da fase 
de verificação, o coordenador identifica a partir dos conjuntos de objetivos alcançáveis de cada agente que o planejamento 
centralizado será adotado. Porém antes de iniciá-lo, o coordenador tentará organizar a menor equipe com capacidades de satisfazer 
todas as metas (vide Algoritmo \ref{alg:hff2})e nesse instantes todas as combinações possíveis de times compostos de 2 a $n$ 
agentes serão testadas até que uma apresente possibilidade de sucesso. Entretanto, o processo de avaliação dessas opções não 
é paralelizado, consequentemente o aumento do número de elementos afeta diretamente o tempo gasto nessa fase devido ao fato da 
quantidade de possibilidades também crescer. Com a paralelização dessa etapa, mais de uma análise pode ser feita simultaneamente, 
garantindo uma melhoria de desempenho. Ainda em relação ao processo de formação das equipes, é importante ressaltar que a 
diferença entre as estratégias de balanceamento de carga e economia foi maior que nos outros testes devido ao não-determinismo da 
escolha da equipe a ser avaliada.

Apesar dessa limitação, o LCMAP apresentou resultados melhores do que os outros modelos mesmo em condições mais complexas. Para a 
configuração com 16 agentes, os tempos gastos foram menores do que aquele apresentado pelo FMAP com apenas 5 agentes, $61,325$ e 
$75,873$ contra $84,025$ segundos, respectivamente.

\input{escalabilidadeRovers}

\input{escalabilidadeLogistics}

\section{Análise dos Resultados}

A fim de avaliar a eficácia e eficiência do LCMAP, experimentos foram realizados e os resultados foram comparados com dois 
modelos com implementações disponíveis, FMAP \cite{fmap} e MADLA \cite{madla}. As análises foram baseadas nas métricas 
utilizadas: (i) tempo total gasto, (ii) tamanho dos planos, e (iii) número de agentes empregados, e na escalabilidade oferecida 
pelo modelo.

Do ponto de vista da capacidade de apresentar uma solução, o modelo proposto conseguiu resolver todos os problemas enquanto os 
demais falharam em pelo menos um caso, o que demonstra sua eficácia. Considerando o tempo gasto, o LCMAP foi mais eficiente em 
$72,72 \%$ dos experimentos e chegou a ser $100\times$ mais rápido. Com relação ao número de agentes empregados, o LCMAP 
conseguiu propor soluções mais econômicas em $36,36\%$ ou $66,67\%$ dos casos, considerando todos os experimentos ou apenas 
aquelas passíveis de economia, respectivamente. Em todos os experimentos, a diferença de tempo gasto entre as estratégias de 
balanceamento de carga (LCMAP-BC) e economia (LCMAP-E) foi pequena porque o processo de atribuição de objetivos (Algoritmo 
\ref{alg:assign}) executa quantidades de operações semelhantes, independente da estratégia. Os melhores resultados obtidos nos 
experimentos dos domínios \textit{satellite}, \textit{rovers} e \textit{logistics} são causados principalmente pelo 
desacoplamento dos agentes e consequente simplificação do processo de planejamento para os dois primeiros casos, e por uma fase 
de verificação ser mais simples na condições propostas pelo problema, no último caso.

Em relação à escalabilidade, ou seja, o impacto do aumento do número de agentes no desempenho 
do LCMAP, o acréscimo no tempo de saída foi menor que a taxa de crescimento do sistema em $84,37\%$ dos 
experimentos para o domínio fracamente acoplado e só não foi preservado para o caso fortemente acoplado devido a uma limitação do 
algoritmo de definição do conjunto mínimo de agentes necessários. Outra importante observação é que as Figuras 
\ref{img:escalabilidadeRovers} e \ref{img:escalabilidadeLogistics} comprovam a complexidade exponencial do tempo necessário para 
solucionar o problema de planejamento multiagente.

Por fim, outra razão que propiciou que o modelo proposto alcançasse melhores resultados foi o processo de coordenação adotado. 
Principalmente em relação ao FMAP, a troca mensagens necessária para coordenar as atividades dos agentes a cada iteração do 
processo de refinamento dos planos configura o maior limitador para a escalabilidade. Por isso, o processo de coordenação mais 
leve adotado pelo LCMAP, no qual as interações entre os agentes ocorrem em momentos bem definidos, mostrou ser mais eficiente do 
que as propostas analisadas. Nesse processo, como os agentes interagem apenas com o coordenador para informar quais objetivos 
podem satisfazer, seus planos calculados ou falhas, o número de troca de mensagem pode ser definido em função do número de 
elementos presentes no sistema. Portanto, essa coordenação explora ao máximo a independência dos agentes, nos casos fracamente 
acoplados, e evita um elevado volume de mensagens adotando uma postura centralizadora para domínios fortemente acoplados.
