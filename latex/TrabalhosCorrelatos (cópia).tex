Existem várias pesquisas relacionadas ao tema de planejamento, cada uma delas lida com diferentes características do domínio do 
problema, tais como níveis de acoplamento de recursos, processo de coordenação, tipos de objetivos e privacidade de informações. 
Nessas abordagens, o processo deliberativo dos agentes é implementado de diversas maneiras o que torna a análise dessa gama de 
possibilidades um desafio na área de pesquisa de MAP.

Os trabalhos destacados na revisão sistemática são detalhados a seguir com o objetivo de identificar suas características, 
contribuições, pontos fracos e principalmente as possibilidades de melhorias. Consequentemente, as decisões tomadas durante o 
desenvolvimento do modelo proposto e apresentado no Capítulo \ref{cap:Proposta} foram motivadas e justificadas por essas 
observações.

Por fim, uma proposta de taxonomia é apresentada a fim de classificar os trabalhos, destacando suas peculiaridades.

\section{A Formal Analysis of Required Cooperation in Multi-Agent Planning}

Em \citet{cooperation} é estudada a necessidade de cooperação entre os agentes com o intuito de garantir condições para que 
objetivos inalcançáveis por um único agente sejam possíveis em um cenário multiagente. A principal questão discutida no trabalho 
é a análise das condições que definem a necessidade da presença de vários agentes na solução de um problema de planejamento. Esse 
tipo de verificação prévia, apesar de ser um ponto pouco explorado em outros trabalhos, é de grande importância porque define 
tipos distintos de uso para MAP, ou seja, situações nas quais vários agentes:

\begin{enumerate}
 \item  são necessários porque não um plano com um único agente que satisfaça todos os objetivos; 
 \item são utéis para melhorar a eficiência da execução, mesmo que um plano com um único agente seja capaz de atingir os 
objetivos.
\end{enumerate}

Esses dois tipos de uso são importantes também para destacar e diferenciar conceitos próximos. Se por um lado, um plano 
multiagente é aquele no qual o papel do agente é de executor, por outro lado em um planejamento distribuído, esse papel pode ser 
de planejador, executor ou ambos.

Além da análise teórica da cooperação requerida entre os agentes, o trabalho apresenta um modelo de planejamento no qual o 
problema original é transformado para utilizar a quantidade mínima de agentes necessários para atingir os objetivos. Apesar dessa 
importante contribuição, inicialmente o modelo é capaz de apresentar o limite superior para o número de agentes quando esses são 
homogêneos, isto é, apresentam as mesmas capacidades.

Os resultados destacam que o modelo proposto é mais rápido que a ferramenta usada como comparação apesar de gerar planos com um 
maior número de ações, consequência direta da transformação do problema. Outra conclusão importante do trabalho é que muitos dos 
casos de testes usados para avaliar planejadores multiagentes são, na realidade, passíveis de solução por um único agente.



% Em \citet{cooperation}, os problemas de planejamento são classificados em heterogêneos e homogêneos. Esta classificação é a 
% condição básica para identificar quando a cooperação é necessária entre agentes, fornecendo uma abordagem formal para descrever um 
% problema como fracamente ou fortemente acoplado. Como consequência direta desse trabalho, quando um problema MAP não apresentar 
% ciclos nos grafos que o representa nem diversividade entre as entidades, é provado que um único agente pode satisfazer todos 
% objetivos. 

\section{FMAP: Distributed Cooperative Multi-Agent Planning}

Uma estratégia presente na literatura é a de computação distribuída e interativa dos planos. Em \citet{fmap}, é apresentado 
um modelo que integra planejamento e coordenação em um ambiente totalmente distribuído, cujas principais vantagens são as 
preocupações com a privacidade da informação e a capacidade de poder ser empregado para solucionar problemas em domínios tanto 
fracamento como fortemente acoplados. 

A construção da solução está baseada no conceito de refinamento, pelo qual um plano-base é progressivamente incrementado – 
adicionando ações – até que o objetivo seja atingido. O processo de refinamento começa com o plano inicial (composto pelo estado 
inicial 
$\alpha_i$ e pelo objetivo $\alpha_f$ a ser 
atendido), que é modificado por todos agentes. Esse plano apresenta a raiz da árvore do espaço de busca que é explorado pelos 
agentes. A Figura \ref{searchTree}  mostra o primeiro nível da árvore gerada para uma determinada tarefa.

Para a exploração da árvore, o FMAP faz uso de uma estratégia de busca A* \cite{russell} e uma liderança democrática 
(\textit{round-robin}). 
Cada interação no processo de busca é composta pelas etapas:

\begin{itemize}
 \item Seleção do plano-base: o agente líder escolhe o plano mais promissor e o envia aos outros agentes; 
 \item Geração do plano de refinamento: cada agente expande o plano-base, gerando os nós sucessores; 
 \item Avaliação do plano de refinamento: cada agente avalia seus planos gerados, fazendo uso de uma função heurística; 
 \item Comunicação do plano de refinamento: cada agente transmite o melhor plano gerado e o resultado da avaliação.
\end{itemize}

\begin{figure}[h]
   \centering
   \includegraphics[width=\textwidth]{images/searchTree.png}
   \caption{Primeiro nível da árvore de busca\cite{fmap}.}
   \label{searchTree}
\end{figure}

Um plano refinado é considerado solução quando ele apresenta o escalonamento de ações capazes de atingir o objetivo final a 
partir do estado inicial.

A principal limitação do FMAP é sua escalabilidade, pois quanto maior o número de agentes envolvidos no planejamento, maior é a 
quantidade de mensagens trocadas entre eles na fase de coordenação. Entre as etapas presentes em cada iteração do método, a única 
gravemente afetada pelo aumento de agentes é aquela referente à comunicação; as etapas de refinamento e avaliação são praticamente 
constantes. 

No entanto, FMAP é a única abordagem das estudas nessa revisão da literatura, que apresenta preocupação com técnicas de execução 
paralela pois os autores afirmam que o procedimento de planejamento é otimizado para aproveitar todo o recurso computacional 
disponível, através da atribuição do mesmo número de \textit{threads} para cada agente. Infelizmente, a implementação da 
paralelização não é explicitamente detalhada.

A avaliação dos resultados destaca que modelo além de ser independente de domínio, isto é, capaz de resolver problemas fraca 
ou fortemente acoplados, também apresenta tempos de execução melhores do que as propostas usadas como comparação.

\section{MAPJA: Multi-agent planning with joint actions}

Um caso especial de planejamento multiagente é quando os agentes devem cooperar para que os objetivos sejam atingidos, uma vez 
que individualmente eles não podem satisfazê-los. Apesar dessa preocupação ter sido explorada em \citet{cooperation}, 
trabalho no qual é apresentada um formulação teórica da necessidade de cooperação, existe ainda um aspecto que é pouco explorado 
na área de MAP.

Em \citet{mapja}, o foco do trabalho é a questão de ações conjuntas (\textit{joint actions}), isto é, agentes executando tarefas 
sobre o mesmo objeto. Sua principal diferença em relação a outros trabalhos que também são capazes de resolver problemas de 
planejamento que envolvam domínios fortemente acoplados nos quais a cooperação é uma exigência é a forma como os conjuntos de 
ações são tratatdos. Enquanto \cite{fmap,madla,extensionfmap} consideram que agentes que possuam o mesmo conjunto de ações 
necessariamente apresentam as mesmas capacidades, o trabalho em pauta assume que mesmo com coleções iguais, os agentes podem 
executar as ações de maneiras distintas. Um exemplo dessas peculiaridades é o fato de dois agentes (robôs) serem capazes de 
levantar blocos, porém com capacidades diferentes, sendo o primeiro limitado a 10 kg e outro a 20 kg.

Em seu modelo, \citet{mapja} explora a transformação do problema original em dois tipos de problemas. Se o novo problema 
não necessitar de ações conjuntas, o problema é então resolvido por um planejamento multiagente existente e disponível. Porém se 
tal condição for necessária, o número de agentes é calculado e o planejamento é realizado de forma centralizada. Diferente de 
outros modelos, esse trabalho não assume nenhum hipótese sobre quais agentes são necessárias e também não envolve todas entidades 
disponíveis no processo de planejamento.

Na fase de avaliação, o modelo é comparado com uma abordagem que transforma um problema MAP em vários problemas menores 
(SAP) que então são combinados na fase final \cite{sap}. Os resultados mostram que o modelo proposto além de apresentar um número 
menor de nós expandidos durante a busca também executa o processo em um tempo menor quando comparado com \cite{sap}. Entretanto, 
os testes consideraram apenas uma quantidade pequena de agentes (2, 3, 4 e 5) o que prejudica a avaliação da propriedade de 
escalabilidade do modelo.

\section{MADLA}

Em \citet{madla} é proposto um planejador de domínio independente que combina dois tipos de heurísticas. Em uma primeira fase, o 
cálculo heurístico baseia-se apenas nas informações de um agente isoladamente, para em seguida, utilizar um compartilhamento de 
dados para estimar o valor.

De maneira similar a \citet{fmap}, esse trabalho apresenta o uso de técnicas de programação paralela através da alocação de uma 
\textit{thread} por agente e também visa manter a privacidade, compartilhando de maneira seletiva as informações. Entretanto, o 
planejamento é feito de maneira centralizada diferentemente do trabalho citado. 

Uma questão interessante apresentada nessa pesquisa é o fato da tradução da entrada em formato MA-STRIPS para PDDL, o que de 
certa forma, destaca uma necessidade de desacoplamento entre as atividades de processamento dos arquivos de entrada e de 
planejamento propriamente dito.

Em relação a avaliação do modelo, o trabalho destaca que a combinação dos tipos de heurísticas aumenta a capacidade de 
solução de problemas porém não apresenta métricas como tempo de execução ou tamanho dos planos.

\section{Multi-Agent Planning by plan Reuse}

Um obstáculo que as abordagens devem buscar superar está relacionado com o processo de coordenação entre os agentes. Se 
eles cooperam sob um controle centralizado, um grande número de mensagens pode ser produzido com o objetivo de garantir e a 
coordenação. Uma possível solução para esse desafio é a transformação do problema de MAP para várias instâncias individuais de 
planejamento (SAP). Essa estratégia tem sido seguida por trabalhos recentes nos quais os objetivos são divididos a distribuição 
deles pelos agentes disponíveis é uma solução em comum, embora diferentes abordagens e técnicas sejam aplicadas \cite{mapr,sap, 
DOMAPS}.

Em \citet{mapr}, a abordagem proposta considera agentes com informações públicas 
e privadas que comunicam-se em uma estrutura em anel. Nesse trabalho, o processo de planejamento é composto por seis etapas:  

\begin{enumerate}
 \item Um subconjunto dos objetivos globais é delegado para cada agente ao mesmo tempo 
que seus objetivos privados são preservados. Em relação as estratégias usados nessa etapa de delegação, as possibilidades são as 
seguintes:

  \begin{itemize}
  \item atribuir cada objetivo a todos agentes caso esse tenha a capacidade de satisfazê-lo;
  \item atribuir todos os objetivos possíveis ao primeiro agente e então removê-los do conjunto. Em seguida, delegar essa nova 
coleção
  à segunda entidade, repetindo esse processo ao terceiro até o n-ésimo agente até que o conjunto de objetivos esteja vazio;
  \item atribuir cada objetivo ao agente que tem melhor condição de satisfazê-lo, ou seja, com um menor custo;
  \item atribuir um número balanceado de objetivos, ou seja, $\frac{|Objetivos|}{|agentes|}$ a cada agente respeitando a 
estratégia 
anterior.
  \end{itemize}

\item O primeiro agente inicia seu processo de planejamento individualizado e caso não consiga resolver o problema 
completamente, 
ele simplesmente repassa um plano vazio ao próximo agente seguindo uma estrutura em anel. Dessa forma, o 
processo de planejamento pode ser repetido mais de uma vez por cada entidade e o ciclo é mantido até que todos objetivos tenham 
sido 
satisfeitos;

\item Caso o primeiro agente tenha resolvido o seu problema, ele mascara suas informações para manter a privacidade sobre elas 
uma vez que essa solução é repassada aos demais elementos;

\item Esse agente comunica seu plano ao próximo nó da rede, caracterizando a etapa de comunicação;

\item O agente que recebeu o plano adiciona suas ações com o intuito de atingir seus objetivos;

\item Caso o último agente do anel não 
tenha encontrado a solução, ele repassa o plano parcial ao próximo agente, ou seja, aquele que iniciou a interação. Caso um plano 
tenha 
sido encontrado, o processo seja ao fim. Para evitar um ciclo 
infinito, são adicionadas limites de consumo de tempo e de memória para indicar situações de falha.

\end{enumerate}

Para promover a avaliação da proposta, os resultados obtidos foram comparados e apresentaram melhor desempenho do que 
\citet{mappop}.

\section{Transforming Multi-Agent Planning Into Single-Agent Planning Using Best-cost Strategy}

A transformação de um problema MAP em vários problemas SAP é uma estratégia comum e explorada em diversos trabalhos \cite{DOMAPS, 
divideconquer, mapr, sap}. Essa transformação é realizada através da delegação de subconjuntos de objetivos a diferentes agentes, 
tornando cada um deles responsável pela solução de problemas menores e possivelmente mais simples de serem resolvidos. 
Entretanto, o ganho que essa transformação traz é pouco ou raramente discutida na literatura.

Como uma exceção a essa regra, \citet{bracis} apresenta uma discussão sobre a economia de recursos que pode ser alcançada quando 
os objetivos são distribuídos entre os agentes disponíveis. O trabalho propõe um divisão dos objetivos considerando o custo total 
do plano que, por sua vez, é calculado pela soma das ações que o compõem. Por fim, a configuração que apresentar a menor soma 
final, isto é, a soma dos custos dos planos de cada agente, é definida como vencedora.

Apesar de realizar avaliação apenas com um domínio fracamente acoplado e com agentes homogêneos (com as mesmas capacidades), da 
mesma maneira que realizado em \cite{cooperation}, os resultados apresentados justificam a importância de explorar a estratégia 
de delegação de objetivos. As métricas de desempenho consideradas foram custo total do plano, tempo de planejamento e uso de 
memória pelo planejador usado, no caso \textit{Graphplan}\cite{graphplan} e as melhoras obtidas foram de $75.66\%$, $96.64\%$ e 
$84.34\%$, respectivamente.

\section{Taxonomia}

%TODO Construir a tabela sobre a presença dos módulos em cada abordagem

A análise dos trabalhos listados nessa revisão de literatura, aliada às questões de detecção de insolvabilidade e execução 
paralela, possibilitou a classificação das abordagens de MAP em relação às características presentes ou desejáveis em cada 
uma delas. 

A primeira característica trata dos arquivos de entrada que definem o domínio e o problema de planejamento. A maioria dos 
trabalhos estudos utiliza linguagens padrões porém \citet{fmap} baseia-se em uma extensão de PDDL o que ratifica a necessidade de 
uma preocupação com a flexibilidade do formato de entrada. Uma possível solução para essa questão seria a adoção de uma linguagem 
pré-definida que seria gerada após um processo de tradução dos arquivos da forma que é realizada em \citet{madla}. Uma vez que as 
entradas sejam traduzidas, faz-se necessária também a presença de um processador (\textit{parser}) capaz de interpretar os novos 
arquivos gerados, identificando a declaração dos elementos tais como estado inicial, ações e objetivos, e em seguida instanciando 
os respectivos objetos.

Uma observação que merece destaque é a inexistência de abordagens que buscam avaliar se um problema pode ser resolvido 
antes de iniciar o processo de planejamento. Apesar disso, a comunidade científica já apontou a necessidade de pesquisas nessa 
direção, fato comprovado pela criação do desafio lançado pela \textit{International Planning Competition 
(IPC)}\cite{unsolvability}. Outro destaque é baixa exploração de técnicas de execução paralela nos trabalhos apresentados. Nesse 
sentido, a segunda característica, desejada pois não foi identificada nessa revisão, é a verificar a possibilidade de solução do 
problema respeitando as condições impostas. De uma forma geral, um problema pode ser impossível de ser resolvido caso os agentes 
disponíveis não possuam ações e capacidades que reflitam as necessidades do desafio. 

A terceira característica é uma consequência direta da anterior e está relacionado com a verificação da necessidade de cooperação 
entre os agentes. Muitas vezes, o domínio descreve uma situação na qual individualmente o problema é impossível de ser resolvido, 
entretanto de maneira conjunta ele passa a ser possível. Portanto, a análise de cooperação, discutida em \citet{cooperation}, e a 
presença de ações conjuntas, estudada em \citet{mapja}, configuram uma classificação importante para os trabalhos.

Com raras exceções, como por exemplo \citet{mapja} e \citet{cooperation}, os modelos de planejamento sempre utilizam todos os 
agentes disponíveis. Se por um lado, a alocação de todos os recursos tende a maximizar a possibilidade de encontrar uma solução, 
por outro lado, isso claramente viola a questão do uso eficiente de recursos além de influenciar diretamente na escalabilidade do 
modelo, como mostrado em \cite{fmap}. Portanto, a quarta característica identificada é seleção dos agentes que serão envolvidos 
no processo de planejamento.

Uma abordagem comum para resolver problemas MAP é a sua transformação em problemas menores, possivelmente mais simples e capazes 
de serem resolvidos por um único agente. Nesse sentido, \citet{mapr} explora diferentes estratégias de delegação de objetivos 
entre os agentes enquanto \citet{bracis} destaca os ganhos obtidos com essa técnica. Consequentemente, a transformação do 
problema original é a quinta característica identificada nessa revisão.

A maneira pela qual o planejamento é conduzido, isto é, a coordenação, também é uma característica importante para classificar os 
trabalhos devido ao fato de alguns seguirem abordagens distintas, isto é, centralizada ou distribuída. Se por um lado a primeira 
opção pode ser mais simples apesar de apresentar um ponto único de falha, a segunda pode ser mais robusta ao custo do prejuízo da 
escalabilidade devido a um elevado número de mensagens trocadas. 

% Igualmente importante é o objetivo que cada trabalho visa atingir, seja ele a execução do plano ou sua construção, conforme 
% destaca \citet{cooperation}. 
A questão da manutenção da privacidade das informações entre os agentes é última característica 
proposta nessa taxonomia.

Finalmente, a taxonomia proposta é composta pelos seguintes termos e valores, conforme Tabela \ref{tab:taxonomia}. 
Os termos \textit{não}, de maneira especial, indicam tanto a ausência explícita da característica quanto a falta de 
citação no trabalho. Em relação às combinações possíveis dos termos, na Figura \ref{fig:taxonomiaClassificacao} é ressaltado que 
as 
% 5 primeiras características (C1 a C5) 
características podem existir independente uma da outra, ou seja, a classificação anterior não influencia na seguinte. As 
características referentes ao teste de solução e análise de cooperação são apresentados justapostos pois fazem parte de uma 
mesma fase de verificação. 
% A classificação relacionada à privacidade só faz sentido em um cenário de coordenação distribuído. 
Do exposto, cabe destacar que o ordenamento vertical e agrupamento apresentados representam a sequência de classificação de um 
trabalho de planejamento multiagente. 

\begin{table}[htbp]
 \centering
 \caption{Taxonomia para trabalhos de MAP.}
 \begin{tabular}{|c|c|c|}
  \hline
  Característica & Termo & Valores possíveis \\ \hline
  C1 & Flexibilidade de entrada & Sim ou Não \\ \hline
  C2 & Teste de solução & Sim ou Não \\ \hline
  C3 & Análise de cooperação & Sim ou Não \\ \hline
  C4 & Seleção de agentes & Sim ou Não\\ \hline
  C5 & Transformação do problema original & Sim ou Não\\ \hline
  C6 & Coordenação & Centralizada ou Distribuída \\ \hline
  C7 & Privacidade & Preservada ou Ignorada \\ \hline
 \end{tabular}
 \label{tab:taxonomia}
\end{table}

A taxonomia proposta pode ser usada também como um \textit{framework} para criação de modelos de planejamento 
multiagente. Sendo assim, o ordenamento passa a identificar as etapas a serem executas, começando pelo tratamento dos arquivos de 
entrada (C1) e passando por uma verificação do problema (C2 e C3, preferencialmente em paralelo). A seleção dos agentes (C4) só é 
viável caso exista a possibilidade de solução e, por sua vez, a transformação do problema dependerá das entidades escolhidas. 
Por fim, as características de coordenação (C5) e privacidade (C6) dependerão do algoritmo/modelo proposto.

\begin{figure}[htbp]
 \centering
 \includegraphics[scale=0.5]{images/taxonomia_classificacao.png}
 \caption{Sequência de classificação.}
 \label{fig:taxonomiaClassificacao}
\end{figure}

Para resumir os trabalhos identificados na revisão sistemática da literatura e detalhados nesse capítulo, a Tabela 
\ref{tab:resumo} sintetiza e classifica-os de acordo com as características da taxonomia proposta.

\begin{table}[htbp]
 \centering
 \caption{Trabalhos classificados de acordo com a taxonomia proposta.}
 \begin{footnotesize}
  
  \begin{tabular}{|c|c|c|c|c|c|c|c|}
    \hline
    \multirow{2}{*}{Trabalho} & \multicolumn{7}{c|}{Características} \\ \cline{2-8}
     & C1 & C2 & C3 & C4 & C5 & C6 & C7 \\ \hline
    \citet{cooperation} & Não & Não & Sim & Sim & Não & Centralizada & Ignorada \\ \hline
    \citet{fmap} & Não & Não & Não & Não & Não & Distribuída & Preservada \\ \hline
    \citet{mapja} & Não & Não & Não & Sim & Sim & Centralizada & Ignorada \\ \hline
    \citet{madla} & Sim & Não & Não & Não & Sim & Centralizada & Preservada\\ \hline
    \citet{mapr} & Não & Não & Não & Não & Sim & Distribuída& Preservada\\ \hline
    \citet{bracis} & Não & Não & Não & Sim & Sim & Centralizada & Ignorada\\ \hline
  \end{tabular}
 \end{footnotesize}
 \label{tab:resumo}
\end{table}


Os módulos, apresentados na Figura \ref{fig:arquitetura}, são componentes responsáveis por instanciar as fases 
destacadas em \cite{introMAP2005, introMAP2009, teamwork}, aliados aos módulos dessa pesquisa (verificadores de insolvabilidade e 
cooperação). Para facilitar o entendimento da análise, esses elementos foram classificados nas seguintes categorias: