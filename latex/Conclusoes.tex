O modelo de planejamento multiagente proposto nesse trabalho, denominado LCMAP, é um modelo de propósito geral, uma vez que é 
capaz de resolver tanto problemas fracamente quanto fortemente acoplados. A motivação principal para seu desenvolvimento foi o 
preenchimento de lacunas identificadas nos trabalhos correlatos, principalmente no tocante à verificação das capacidades dos 
agentes e à seleção dos agentes envolvidos no processo de planejamento. Portanto, o modelo apresentado apoia-se em fases de 
verificação e transformação do problema, e na validação de planos. 

No modelo proposto, os agentes são caracterizados por arquivos de domínio que definem suas capacidades a partir de modelos de 
ação, por arquivos de problema que descrevem o conhecimento que cada um tem do ambiente, e por uma ferramenta de planejamento 
automatizado que garante a autonomia deliberativa.

Uma característica que distingue o LCMAP dos demais trabalhos é o fato de ele explorar as duas dimensões do planejamento 
multiagente: agentes como executores ou planejadores. Algumas pesquisas propõem soluções para o processo de busca do plano, seja 
de forma centralizada ou distribuída, e 
portanto atribuem ao agente o papel de planejador \cite{mapr, madla, fmap}. Em outros casos, o agente é visto como um executor e 
a análise é feita considerando a execução dos planos, verificando quando uma cooperação entre os agentes é necessária e se 
os planos são independentes entre si, não importando como o planejamento foi realizado \cite{cooperation,petrinets}. Nessa 
perspectiva, o LCMAP considera o agente tanto como planejador quanto executor, delegando conjuntos de objetivos a cada um deles 
para que deliberem de maneira independente entretanto, garantindo que realizem os planos sem qualquer tipo de competição ou 
concorrência. 

Durante a fase de verificação, os agentes verificam suas capacidades utilizando uma adaptação 
da heurística \textit{delete-relaxation} para estudar se os efeitos causados pelos operadores, isto é, ações 
instanciadas, são capazes de produzir um estado no qual todos ou parte dos objetivos são satisfeitos. Essa propriedade é uma 
importante contribuição para a área de pesquisa porque os trabalhos correlatos, geralmente, assumem como hipótese inicial o fato 
que qualquer agente é capaz de atender a qualquer objetivo, premissa que facilmente é negada em problemas do mundo real.

Já na fase de transformação, o coordenador busca selecionar os agentes capazes de resolver alguma parte do problema diminuindo 
assim o número de elementos envolvidos. Em seguida, os objetivos são distribuídos entre os candidatos escolhidos seguindo 
estratégias que visam o balanceamento de carga ou a economia de recursos. Dessa forma, o problema original (MAP) é transformado 
em instâncias menores (SAP) e possivelmente mais simples. 

Uma vez que os objetivos foram distribuídos, cada agente selecionado inicia seu planejamento individualmente, e é nesse ponto que 
o processo de coordenação adotado mostra ser mais eficiente. Ao delegar tarefas aos agentes de tal forma que eles possam 
deliberar suas ações sem interagir entre si, o coordenador garante que não há necessidade de troca de mensagem entre eles para 
controlar as atividades. Por outro lado, caso a transformação não seja possível, o coordenador, após de definir a menor 
equipe necessária, assume a responsabilidade de encontrar um plano utilizando o menor número possível de agentes. 

A fase de validação consiste na verificação da independência dos planos, ou seja, se as sequências de ações encontradas de 
maneira desacoplada pelos agentes planejadores realmente podem ser executados de maneira paralela. Para isso, o coordenador 
aguarda a resposta de todos os agentes para analisar as interseções dos planos e ter a certeza que não há qualquer tipo de 
concorrência ou competição.

Essas fases foram importantes para responder à questão de pesquisa definida: \textit{Quais são os efeitos da execução de um 
pré-planejamento para um planejamento multiagente?}. A execução dessas fases favorecem tanto uma redução do tempo gasto durante 
o planejamento quanto uma escalabilidade maior do modelo. Dessa forma, é possível enfrentar a complexidade apresentada em 
problemas de MAP, que se caracteriza por ser exponencial.

Por essas características, o LCMAP apresenta-se como uma contribuição para o estado da arte da área de pesquisa de 
planejamento multiagente, seja por conseguir resultados melhores nas métricas citadas ou por preencher lacunas presentes em 
trabalhos correlatos identificados na revisão da literatura.

\subsection*{Divulgação da Pesquisa}

Os resultados obtidos através dessa pesquisa foram publicados conforme segue:

\begin{itemize}
  \item \textit{Transforming Multi-Agent Planning Into Single-Agent Planning Using Best-cost Strategy}. In Proc $5^{th}$ 
Brazilian Conference On Intelligent Systems (BRACIS) (Qualis B2) - apresenta uma discussão sobre a economia de recursos que pode 
ser alcançada quando os objetivos são distribuídos entre os agentes disponíveis. O trabalho propõe uma divisão dos objetivos 
considerando o custo total do plano que, por sua vez, é calculado pela soma das ações que o compõem.

  \item \textit{Improving Multi-Agent Planning with Unsolvability and Independent Plan Detection}. In Proc $6^{th}$ 
Brazilian Conference On Intelligent Systems (BRACIS) (Qualis B2) - apresenta um modelo de planejamento multiagente baseado na 
alocação dos objetivos considerando as capacidades dos agentes. O modelo foi valido em domínios fracamente acoplados do IPC 
(\textit{satellite} e \textit{rovers}).

  \item \textit{Distributing goals in multi-agent planning through agent’s capabilities}.
  International Conference on Automated Planning and Scheduling (ICAPS) (Qualis A2, em avaliação) - apresenta um modelo de 
planejamento multiagente capaz de resolver problemas de domínios fracamente e fortemente acoplados do IPC mantendo a alocação dos 
objetivos de acordo com as capacidades dos agentes. A validação mostrou que o modelo apresenta resultados de desempenho que 
sobrepõe resultados obtidos com outros trabalhos de estado-da-arte (FMAP 2014, MADLA 2015).
 \end{itemize}

\subsection*{Trabalhos Futuros}

Entretanto, algumas oportunidades de melhoria destacam-se como opções de trabalhos futuros. 
Inicialmente, vislumbra-se sua extensão na direção de modelos de planejamento probabilísticos e dinâmicos, nos quais os efeitos 
das ações não são determinísticos e o ambiente pode ser alterado por eventos externos alheios às ações dos agentes. Outros 
domínios de planejamento podem ser avaliados, da mesma forma que outros planejadores podem ser utilizados pelos 
agentes a fim de 
comprovar a flexibilidade e modularidade do modelo. Por fim, plataformas computacionais com um maior nível de paralelismo podem 
ser usadas como ambientes de teste a fim de avaliar os ganhos obtidos pelo modelo quando mais agentes forem instanciados 
simultaneamente.