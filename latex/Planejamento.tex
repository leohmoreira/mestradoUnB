Planejamento de forma geral é um processo que visa definir uma sequência de ações cujo objetivo é atingir um conjunto de estados 
desejáveis. 
As entradas para a execução desse processo, tais como estados iniciais, recursos disponíveis e restrições afetam a complexidade da 
computação das etapas e consequentemente o tempo de reposta. Em Inteligência Artificial, este tópico é chamado de Planejamento 
Automatizado \cite{ghallab2016}.

Os algoritmos de planejamento automatizado podem ser classificados de acordo com a forma pela qual o espaço de busca é explorado. 
Em uma abordagem de espaço de estados (\textit{state-space}), cada nó desse espaço representa um estado do ambiente e um plano é o 
caminho entre os nós. A transição entre os estados é realizada pela execução das ações, cuja ordenação é dada pelo próprio plano. 
A definição formal de estado será apresentada no prosseguimento do texto.

Na abordagem de espaço de planos (\textit{plan-space}), a busca é baseada em planos parciais, isto é, conjunto de ações 
parcialmente ordenadas. A cada estágio de exploração, os planos são atualizados pela adição de novas ações ou por restrições, até 
que uma solução seja encontrada ou todo espaço tenha sido avaliado. A busca pela sequência de ações, que é capaz de transformar o 
estado inicial nos objetivos finais, pode ser vista como um refinamento do conjunto de possíveis sequências denominadas planos 
parciais \cite{introMAP2005}. Um planejador pode reduzir o número de planos possíveis neste conjunto pela adição de ações, de 
condições que 
permitam a execução de ações e de restrições para um ou mais planos parciais. A esse processo de redução é dado o nome de 
refinamento de plano.

De forma geral, as abordagens de planejamento executam seus algoritmos a partir de tuplas compostas por conjuntos de estados, 
ações e eventos, além de uma função de transição entre esses estados. A abordagem seguida nesse trabalho é a mesma de um sistema 
de transição de estados, cujo formalismo é apresentado em \citet{ghallab2016} e descrito na Definição \ref{def:spaceState}.

\begin{defn}
 \label{def:spaceState}
 Um sistema de transição de estados é uma tupla $\varSigma = (S,A,\gamma)$, onde: 
 \begin{itemize}
  \item $S$: um conjunto finito de estados que definem o ambiente;
  \item $A$: um conjunto finito de ações que podem ser executadas (no prosseguimento do texto será visto que as ações 
são executadas por agentes);
  \item $\gamma: S \times A \rightarrow S$: uma função que a partir de um estado e uma ação leva a um novo estado.
 \end{itemize}
\end{defn}

Além dos itens $S, A, \gamma$, outro conceito utilizado é o de custo da transição de estado para outro através da 
execução de uma ação. Formalmente, o custo é apresentado como uma função $custo: S \times A \rightarrow [0,\infty)$.

Quando a propriedade de finitude dos conjuntos é combinada com restrições adicionais, um modelo de planejamento pode ser 
definido, 
assumindo hipóteses, como por exemplo aquelas definidas em \citet{ghallab2016}:

\begin{enumerate}
\label{classicalPlanning}
  \item O conjunto finito $S$ de estados;

  \item O conjunto de estados $S$ são considerados \textit{totalmente observáveis}, o que indica que é possível ter conhecimento 
sobre 
qualquer estado do ambiente;

  \item O conjunto $A$ de ações \textit{determinísticas}, isto é, os efeitos causados pelas suas execuções são sempre as 
mesmas, sem qualquer 
incerteza;

  \item O conjunto de eventos é considerado \textit{estático} e vazio, isto é, as mudanças no ambiente são causadas estritamente 
pela execução de ações;

  \item Os objetivos são \textit{restritos} o que garante que os planejadores lidem apenas com estados finais explicitamente 
definidos, ignorando qualquer condição ou restrição adicional sobre eles;
  \item A solução, isto é, o plano final, sempre é uma sequência \textit{finita e ordenada} de ações;
  \item A duração da execução das ações e da ocorrência dos eventos (apesar de ter um conjunto vazio) são \textit{instantâneas}, 
ou seja, a transição de estado causada a partir deles (ações e eventos) é imediata; 
  
  \item O planejamento é realizado de forma \textit{offline} o que implica que qualquer mudança no conjunto de estados seja 
ignorada durante o processo de busca pelo plano;

  \item Existe apenas um agente.
\end{enumerate}

% Qualquer abordagem de planejamento que siga essas nove primeiras hipóteses é dita \textit{clássica} (\textit{Classical 
% Planning}).
Tradicionalmente uma abordagem de planejamento que siga essas nove hipóteses é dita \textit{clássica} 
(\textit{Classical Planning}).

No espaço de estados, cada elemento $s \in S$ define propriedades dos diversos objetos que pertencem e definem o ambiente. Dessa 
maneira, é comum utilizar tais propriedades para descrever o cenário de planejamento. Um exemplo simples é a 
caracterização da navegabilidade em um ambiente através da definição de objetos referentes às posições ou coordenadas e das 
relações entre eles, isto é, se são adjacentes, visíveis e alcançáveis entre si. Para facilitar o entendimento a Figura 
\ref{fig:relacaoRigida} apresenta um cenário hipotético composto por quatro posições (A, B, C e D) e pelas relações entre elas. 
Por exemplo, uma aresta direcionada de A para B representa que A está relacionado (adjacente, visível ou alcançável) com B. Nas 
condições descritas e supondo que para deslocar-se de um ponto a outro sejam necessárias a ocorrência das três relações, o 
único movimento possível é alcançar B a partir de A.

\begin{figure}
 \centering
 \includegraphics[width=0.9\textwidth]{images/relacaoRigida.png}
 \caption{Relações e objetos do cenário de planejamento.}
 \label{fig:relacaoRigida}
\end{figure}

Um fato importante dessas propriedades que definem cenários é a possibilidade de serem imutáveis, ou seja, elas são mantidas em 
qualquer estado de $S$. Para formalizar esse conceito é necessário definir variável de estado, que segundo \citet{ghallab2016} 
pode ser descrito conforme a Definição \ref{def:variavelEstado}.

\begin{defn}
 Uma variável de estado é um termo $x = id(o_1, o_2,o_3,\ldots,o_n)$, onde $id$ representa um identificador e cada $o_i$ é um 
objeto pertencente ao ambiente.
\label{def:variavelEstado}
\end{defn}

Cabe ressaltar que as variáveis de estado podem ser entendidas tanto como proposições cujas imagens são valores lógicos de 
\textit{Verdadeiro} e \textit{Falso} quanto funções mapeadas em outros objetos. Dessa forma, na Figura \ref{fig:relacaoRigida}, 
as variáveis de estado são por exemplo $adjacente(A,B)$, $visivel(A,C), alcance(D,A)$, quando utilizadas como proposições 
lógicas. No segundo caso, apenas como ilustração, as variáveis podem ser $adjacente(A)=\{B,D\}$, $visivel(C) =\{D\}, 
alcance(B)=\{A,C\}$.

Uma função que mapeia cada variável de estado $x$ a um valor da sua imagem é definida como \textit{função de atribuição de 
variável}. Com isso, segundo \citet{ghallab2016}, o conceito de \textit{espaço de variável de estado} pode ser formalmente 
descrito conforme a Definição 
\ref{def:espacoVariavelEstado}.

\begin{defn}
 Um espaço de variável de estado é um conjunto $\nu$ de funções de atribuição de variável sobre um conjunto de variáveis de 
estado 
$X$. Cada uma das funções em $S$ é definida como um \textit{estado} pertencente a $S$.
\label{def:espacoVariavelEstado}
\end{defn}

A Figura \ref{fig:relacaoRigida} representa um estado $s_0 = \{adjacente(A,B) = Verdadeiro, \ldots,$ $alcance(D,A)= Verdadeiro\}$ 
no qual existem as seguintes variáveis de estado e funções de atribuição:

\begin{itemize}
 \item Variáveis de estado: 
  \subitem $adjacente(A,B)$;
  \subitem $adjacente(A,D)$;
  \subitem $adjacente(B,C)$;
  \subitem $visivel(A,B)$;
  \subitem $visivel(A,C)$;
  \subitem $visivel(C,D)$;
  \subitem $alcance(A,B)$; 
  \subitem $alcance(B,A)$;
  \subitem $alcance(B,C)$;
  \subitem $alcance(D,A)$;
 \item Funções de atribuição: 
  \subitem $adjacente(A,B) =Verdadeiro$;
  \subitem $adjacente(A,D)=Verdadeiro$;
  \subitem $adjacente(B,C)=Verdadeiro$;
  \subitem $visivel(A,B)=Verdadeiro$;
  \subitem $visivel(A,C)=Verdadeiro$;
  \subitem $visivel(C,D)=Verdadeiro$;
  \subitem $alcance(A,B)=Verdadeiro$; 
  \subitem $alcance(B,A)=Verdadeiro$;
  \subitem $alcance(B,C)=Verdadeiro$;
  \subitem $alcance(D,A)=Verdadeiro$;
\end{itemize}

No caso das proposições que não estão explicitamente descritas, assume-se que às variáveis de estados são atribuídos o valor 
$Falso$, definindo o que conhecido como \textit{hipótese do mundo fechado} \cite{livro}. Dessa forma, o conjunto de funções de 
atribuição do 
estado $s_0$ possui ainda $adjacente(B,A) = Falso, visivel(B,D) = Falso, etc$.

Retornando à discussão a cerca das propriedades entre os objetos e estendo-a às definições de estado e funções de atribuição de 
variável, é possível formalizar o conceito de rigidez apresentado em \citet{ghallab2016} conforme a Definição 
\ref{def:relacaoRigida}. 
 
 \begin{defn}
  Seja $r$ uma relação entre objetos do ambiente de planejamento em um estado de $S$ e $f(x) \in S$ uma função de atribuição 
da variável de estado $x$, $r$ é dita rígida (ou invariante) se para $\forall s_i \in S$, $r$ se mantém inalterada. De forma 
análoga, $f(x)$ é 
dita rígida se $f(x) = constante$ para qualquer $s_i \in S$.
 \label{def:relacaoRigida}
 \end{defn}

Assumindo, por exemplo, um conjunto de estados $S=\{s_0, s_1, \ldots, s_n\}$ e que a função de atribuição $adjacente(A,B) = 
Verdadeiro$ 
seja mantida em qualquer $s_i \in S$, essa função apresenta a propriedade de rigidez.

O segundo item da Definição \ref{def:spaceState} trata do conceito de ação. Porém, antes de defini-lo é necessário formalizar 
\textit{literal} ou \textit{átomo}, de acordo com a Definição \ref{def:atomo} apresentada em \citet{ghallab2016}.

\begin{defn}
 Um literal ou átomo é uma expressão contendo $r(o_1,o_2,\ldots,o_n)$ ou \\$id_v(x_1,x_2,\ldots,x_m) = x_0$, onde $r$ é uma 
relação entre objetos, $id_v$ o identificador de alguma variável de estado e $x_i$ representa variáveis que podem assumir 
qualquer valor ou constante, com exceção a uma variável de estado.
\label{def:atomo}
\end{defn}

Quando em um literal todas as ocorrências de variáveis $x_i$ foram substituídas por constantes ou objetos do domínio, ou seja, 
não existem variáveis, esse átomo é chamado de proposição ou instância (\textit{ground}); caso contrário ele é classificado como 
não instanciado (\textit{unground}). 

Um modelo de ação (ou \textit{template}) pode ser construído a partir de literais \textit{unground} e quando todas variáveis 
forem substituídas por valores, uma ação é dita instanciada a partir do modelo, contendo apenas átomos do tipo \textit{ground}. À 
instância da ação é dada o nome de \textit{operador} \cite{ghallab2016}. 
\begin{defn}
Um modelo de ação $\alpha$ é da forma: 
$\alpha = <def(\alpha), pre(\alpha), eff(\alpha)>$, 
% \end{center}
onde:
\begin{itemize}
 \item $def(\alpha)$ define um nome para a ação e um conjunto de parâmetros, isto é, variáveis;
 \item $pre(\alpha)$ representa um conjunto finito de literais indicando condições para a execução da ação, ou simplesmente, um 
conjunto de pré-condições que devem ser satisfeitos no estado em que a ação for executada;
 \item $eff(\alpha)$ representa um conjunto finito de efeitos, cada um da forma \\$id_v(x_1, \ldots, x_j) \leftarrow x_0$ e onde
 a variável de estado descrita assumirá o valor $x_0$ após a execução da ação.
\end{itemize}
\label{def:acao}
\end{defn}

Adicionado ao cenário descrito na Figura \ref{fig:relacaoRigida} o literal $atual(obj) = pos1$, que representa a posição 
atual do objeto, um exemplo de modelo de ação que descreve a capacidade de um objeto atualizar sua posição pode ser descrito a 
seguinte forma:
% Como ilustração e aproveitando\footnote{Foi adicionado o literal $atual(obj) = pos1$ para descrever a posição atual do objeto.} o 
% cenário descrito na Figura \ref{fig:relacaoRigida}, um exemplo de modelo de ação que descreve a capacidade de um objeto atualizar 
% sua posição de X para Y pode ser descrito a seguinte forma:

\begin{center}
$mover = <def(mover), pre(mover), eff(mover)>$, 
\end{center}
onde:
\begin{itemize}
 \item $def(mover) = mover(obj, pos1, pos2)$;
 \item $pre(mover) = \{adjacente(pos1,pos2), visivel(pos1,pos2), alcance(pos1,pos2)$,\\$atual(obj) = pos1\}$;
 \item $eff(mover) = \{atual(obj) \leftarrow pos2\}$.
\end{itemize}

Conforme apresentado na Definição \ref{def:acao}, o conjunto $pre(\alpha)$ determina quais condições devem ser satisfeitas para 
que uma instância daquela ação possa ser executada. Portanto, para o operador $\alpha$ ser aplicável em um estado $s$ é preciso 
que todos literais determinados por $pre(\alpha)$ sejam satisfeitos pelas funções de atribuição de variável de estado presentes em 
$s$. Quando $s$ satisfaz $pre(a)$, a transição de estado causada pelo operador $a$ é definida pela Equação \ref{eq:transicao}. 
Consequentemente, o último elemento apresentado na tupla da Definição \ref{def:spaceState}, o qual define um sistema de 
transição, isto é, a função $\gamma$, está formalizado conforme \cite{ghallab2016}.

\begin{footnotesize}
  \begin{equation}
  \gamma(s,a) = eff(a) \cup \{id_v(x_i, \ldots, x_j) = x_0 \in s | id_v(x_i, \ldots, x_j) \leftarrow x_k \notin eff(a), x_k \neq 
  x_0\}
  \label{eq:transicao}
  \end{equation}
\end{footnotesize}

Dessa forma, considerando o estado $s_0$ descrito na Figura \ref{fig:relacaoRigida} e o operador \\$mover(obj,A,B)$ tem-se que o 
novo estado $s_1 = \gamma(s_0, mover)$ será a união de \\$\{atual(obj)=pos2\}$ com todas demais variáveis de estado inalteradas.

De posse do formalismo herdado dos sistemas de transição é possível especializar 
os conceitos para o cenário de planejamento automatizado e definir \textit{domínio de planejamento} conforme a Definição 
\ref{def:dominio} apresentada em \citet{ghallab2016}.

\begin{defn}
 Um domínio de planejamento é uma tupla $\varSigma_D = <S,O,\gamma>$, onde $O$ é o conjunto de todos operadores instanciados a 
partir de um conjunto $A$ de modelos de ação.
\label{def:dominio}
% \cite{ghallab2016}
\end{defn}

Conforme foi apresentado na Seção \ref{planejamento}, a finalidade do planejamento automatizado é definir uma sequência de 
ações possíveis para atingir um conjunto de estados desejáveis. 
Entretanto, de acordo com as definições supracitadas, essa sequência é composta de operadores, isto é, instâncias de ações. Logo, 
a solução para um problema de planejamento clássico é um plano $\pi = <a_1, a_2, \ldots, a_n>$, onde $ a_i \in O$ é uma sequência 
finita de operadores.

Um problema de planejamento clássico que pode ser caracterizado por um estado inicial e final além das 
regras de transformação. A Definição \ref{def:problema} constante de \citet{ghallab2016} apresenta o conceito de planejamento 
automatizado.
\begin{defn}
Um problema de planejamento é uma tupla $P = <\varSigma_D,s_o,g>$, onde $\varSigma_D$ é um domínio de planejamento, $s_0$ é o 
estado inicial e $g$ é um conjunto de literais que devem ser satisfeitos nos estados meta.
\label{def:problema}
\end{defn}

Portanto, a finalidade do planejamento automatizado é calcular um plano $\pi$, tal que $\gamma(s_{i-1},a_i) = s_i$ , para $i = 1, 
2, 
\ldots, n$ tal que $s_n$ satisfaça todos elementos de $g$.

Independente da abordagem adotada para representar o ambiente, seja a de espaço de estados ou de planos, o processo de busca da 
solução pode ser executado a 
partir do estado inicial em direção aos objetivos ou de forma inversa, caracterizando as técnicas de busca progressivas 
(\textit{forward}) e regressivas (\textit{backward}), respectivamente apresentadas nos Algoritmos \ref{alg:forward} e 
\ref{alg:backward}, apresentados em \citet{ghallab2016}.

\begin{algorithm}
 \caption{Busca progressiva}
  \small
  \label{alg:forward}
  \begin{algorithmic}[1]
   \Require $\varSigma_D, s_0, g$
    \Ensure Plano $\pi$ ou falha
      \State $Fronteira \gets \{(<>,s_0)\}$ \Comment{Tupla formada pelo plano vazio e estado $s_0$}
      \State $Visitados \gets \emptyset$
      \While {$Fronteira \neq \emptyset $}
	\State Selecione um nó $v = (\pi, s) \in Fronteira$
	\State $Fronteira \gets Fronteira - \{v\}$
	\State $Visitados \gets Visitados \cup \{v\}$
	\If{$s$ satisfizer $g$}
	  \Return $\pi$
	\EndIf
	\State $Novos \gets \{(\pi.a,\gamma(s,a))|s$ satisfaz $pre(a)\}$
	\State Remova elementos de $Fronteira, Visitados, Novos$ 
	\State $Fronteira \gets Fronteira \cup Novos$
      \EndWhile
      \Return Sem solução
  \end{algorithmic}
\end{algorithm}

Na Linha 9 do Algoritmo \ref{alg:forward} é utilizada a operação de concatenação de uma nova ação ao plano. Dessa forma, 
considerando um plano $\pi=<a_1,a_2, \ldots, a_n>$ e um operador $a_m$, a operação $\pi.a$ resulta em $\pi=<a_1,a_2, \ldots, 
a_n, a_m>$.

\begin{algorithm}
 \caption{Busca regressiva}
  \small
  \label{alg:backward}
  \begin{algorithmic}[1]
   \Require $\varSigma_D, s_0, g$
   \Ensure Plano $\pi$ ou falha
      \State $\pi \gets <>$
      \State $goal \gets g$
      \Loop
	\If{$s_0$ satisfizer $goal$}
	  \Return $\pi$
	\EndIf
	\State $Ops \gets \{o \in O | g \in eff(o) \}$
	\If {$Ops = \emptyset$}
	  \Return Sem solução
	\EndIf
	\State Escolha $o \in Ops$
	\State $goal \gets \gamma^{-1}(goal,o)$
	\State $\pi \gets a.\pi$
      \EndLoop
      \Return Sem solução
  \end{algorithmic}
\end{algorithm}

O algoritmo regressivo, por explorar o espaço de busca a partir dos objetivos, utiliza a função de regressão $\gamma^{-1}$ que 
calcula o estado a partir do qual é possível atingir um dos elementos de $g$. Formalmente, a função é apresentada pela Equação 
\ref{eq:regressao} apresentada em \citet{ghallab2016}.

\begin{equation}
 \label{eq:regressao}
 \gamma^{-1}(g,a) = pre(a) \cup \{ id_v(x_1, x_2, \ldots, z_n) \in g |id_v(x_1, x_2, \ldots, z_n) \notin eff(a) \}
\end{equation}
% \cite{ghallab2016}

Para otimizar o processo de exploração do espaço de busca, os algoritmos de planejamento utilizam diversos tipos de funções 
heurísticas para guiar as escolhas feitas em cada iteração. As funções heurísticas são usadas nos algoritmos especialmente na 
etapa de remoção e seleção de elementos dos conjuntos, indicadas na Linha 10 do Algoritmo \ref{alg:forward} e Linha 9 do 
Algoritmo \ref{alg:backward}. 

Uma das possibilidades de função heurísticas é baseada na noção de \textit{delete-relaxation}, ou seja, um relaxamento do 
problema no qual a execução de uma ação nunca remove literais antigos de um estado, mas simplesmente adiciona novos 
\cite{ghallab2016}. Nesse contexto, alguns conceitos surgem naturalmente e são apresentados nas Definições 
\ref{def:estadoRelaxado}, \ref{def:rsatisfaz} e \ref{def:raplicavel}.

\begin{defn}
 Um estado relaxado $\hat{s}$ é um conjunto de literais do tipo \textit{ground} tal que toda variável de estado do cenário de 
planejamento está presente em pelo menos um literal.
\label{def:estadoRelaxado}

\end{defn}

\begin{defn}
 Um estado relaxado $\hat{s}$ satisfaz um conjunto de literais $g$ se existir um subconjunto $s \subseteq \hat{s}$ que satisfaz 
$g$.
\label{def:rsatisfaz}

\end{defn}

\begin{defn}
 Um operador a é aplicável em um estado relaxado $\hat{s}$ se $\hat{s}$ satisfaz pre(a). A transição a partir de um estado 
$\hat{s}$ aplicando um operador a é definido pela Equação \ref{eq:gammahat}:
 \begin{equation}
 \label{eq:gammahat}
  \gamma^+(\hat{s},a) = \hat{s} \cup \gamma(s,a)
 \end{equation}
 \label{def:raplicavel}
\end{defn}

Embora a hipótese de ações determinísticas simplifique o processo de planejamento automatizado, ela pode impedir a representação 
de um 
problema real. Sendo assim, entre os problemas em aberto na área de planejamento, o uso de modelos 
probabilísticos merece destaque. Nesse contexto, os efeitos causados pelas ações devem estar associadas à distribuições de 
probabilidade \cite{do2012planner}. 

Com a abordagem probabilística, o plano produzido não é uma solução absoluta, mas sim uma sequência de ações com certa chance de 
atingir os objetivos. 
Esses modelos, denominados \textit{Markov Decision Process (MDP)}, englobam diversos problemas tais como diagnóstico médico, tratamento de 
doenças, navegação de robôs, gerenciamento logístico com demanda incerta. O planejamento probabilístico pode tornar-se ainda mais 
desafiador, uma vez que pode ser difícil ou impossível determinar as distribuições de probabilidades que descrevam o comportamento do 
ambiente. Nesse caso, o MDP é reclassificado em \textit{Markov Decision Processes with imprecise probabilities (MDP-IPs)}, onde as funções 
de transição são parametrizadas e subjeitas à restrições \cite{ilao}. 

Como o foco desse trabalho é o planejamento clássico, o modelo probabilístico não será explorado de forma mais detalhada.

\section{Linguagens e Domínios de Planejamento}

O uso de uma boa linguagem de planejamento é um dos principais fatores para um processo de planejamento eficiente. 
Pode-se citar como uma linguagem tradicional em planejamento a \textit{STanford Research Institute Problem Solver (STRIPS)} 
\cite{strips}. A linguagem STRIPS propõe um modelo simples e compacto para especificar os domínios e problemas de planejamento, 
incluindo $\varSigma_D$ (Definição \ref{def:dominio}) e 
$P$ (Definição \ref{def:problema}), respectivamente. Essa simplicidade implica em algumas restrições na representatividade da 
linguagem, como por exemplo:
\begin{itemize}
 \item o estado inicial e o conjunto de pré-condições apenas aceitam literais positivos, isto é, com valor lógico $Verdadeiro$;
 \item não é permitida a disjunção de literais nos objetivos, pré-condições e efeitos;
 \item somente literais \textit{ground} são permitidos nos objetivos;
 \item falta de definição de tipos.
\end{itemize}

Como consequência, a linguagem STRIPS tem sido evoluída e estendida visando aumentar a capacidade de representação de 
problemas de planejamento. Nessa direção, \textit{Action Description Language (ADL)} \cite{adl} usa um modelo algébrico para 
definir os estados do mundo e estende STRIPS oferecendo:
\begin{itemize}
 \item definição de tipos para objetos e parâmetros dos modelos de ações, o que favorece a compreensão dos problemas;
 \item uso de negação de literais nos objetivos e pré-condições;
 \item disjunção de fórmulas.
\end{itemize}

Apesar de ADL ser bem popular, a extensão mais bem sucedida é a \textit{Planning Domain Definition Language (PDDL)} \cite{pddl2}, 
que tem sido a linguagem referência na comunidade (\textit{International Planning Competition - IPC} 
\footnote{\url{http://icaps-conference.org/ipc2008/deterministic/PddlResources.html}}) além de ser 
adotada pela maioria dos planejadores. A linguagem PDDL oferece um conjunto de características, das quais destacam-se:

\begin{itemize}
 \item modelagem das ações baseada em STRIPS;
 \item ações hierarquizadas;
 \item axiomas de domínios que são fórmulas lógicas que estabelecem relações entre os fatos que são satisfeitos em um estado (ao 
contrário de ações, as quais definem relações entre estados sucessivos).
\end{itemize}

Da mesma maneira que na linguagem STRIPS, extensões foram realizadas a partir de PDDL, gerando as seguintes versões:

\begin{itemize}
 \item PDDL2.1 \cite{pddl21} - melhora a expressividade de PDDL adicionando gerenciamento de tempo e capacidades numéricas. 
Entre suas contribuições, destacam-se:
  \begin{itemize}
  \item a proposta de uma sintaxe definitiva que permite comparações entre expressões numéricas;
  \item a especificação de ações com duração discreta e contínua;
  \item a definição de métricas, expressões numéricas ou temporais, para avaliar um problema.
  \end{itemize}
  
 \item PDDL2.2 \cite{pddl22} - permite a definição de predicados que não são afetados por nenhuma ação e também de literais 
que tem seus valores lógicos atribuídos em instantes estabelecidos, independentemente da execução de uma ação;

 \item PDDL3.0 \cite{pddl30} - estende a capacidade de expressar a qualidade de um plano introduzindo características que 
permitem definir condições que devem ser satisfeitas por um sequência de estados durante a execução do plano. Além disso, é 
possível priorizar tais condições;

 \item PDDL3.1 \cite{pddl31} - enriquece a linguagem com a possibilidade de representar questões de satisfatibilidade, de 
utilizar variáveis de estado mapeadas em conjunto finito de objetos, definidos por um tipo e de atribuir custos às ações.
\end{itemize}


As linguagens de planejamento são utilizadas para descrever dois tipos de arquivos. No arquivo de domínio, os modelo de ações, 
predicados e variáveis de estados são listados. O arquivo de problema expressa o estado inicial, isto é, a percepção do ambiente 
em um primeiro momento. Os objetivos e os objetos sobre os quais as ações são executadas também são descritos nesse arquivo. 
% Não há nenhuma anotação explícita sobre os objetivos, portanto eles não são classificados como individuais ou globais. 

Com relação aos domínios de planejamento automatizado, existe uma coleção de casos de teste que é utilizada pela comunidade 
científica para a avaliação de desempenho dos planejadores. Esses testes, conhecidos como \textit{domínios do IPC}, são baseados 
em problemas reais de planejamento que tem sido explorados por competições ao longo dos anos. Novos testes são atualizados a cada 
vez que competições são realizadas. A primeira delas ocorreu em 1998 e apresentou sete domínios, basicamente relacionados a 
questões de logística e deslocamento. Maiores detalhes sobre esses e outros domínios do IPC, obtidos de \cite{jair20}.

Entre alguns domínios do IPC, destacam-se:

\begin{itemize}
 \item \textit{Satellite} - escalonamento de observações de satélites que incluem a coleta e o armazenamento de dados usando os 
diferentes instrumentos para observar uma coleção de alvos;

 \item \textit{Rovers} - uso de um conjunto de robôs para percorrer pontos do planeta realizando uma variedade de operações de 
coleta de amostras e transmissão de dados para a base;

 \item \textit{Zeno-travel} - relacionado a uma questão de transporte aéreo de pessoas;
 
 \item \textit{Elevators} - uso de diferentes tipos de elevadores para deslocamento de pessoas;

 \item \textit{Depots} e \textit{Logistics} - execução de atividades logísticas relacionadas a armazenamento, transporte e 
entrega de produtos.
\end{itemize}




