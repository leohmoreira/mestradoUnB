De uma maneira simples, planejamento multiagente pode ser entendido como o processo de planejamento na presença de múltiplos 
agentes \cite{jonsson2011scaling}. Entretanto, essa definição não torna claro quais papéis os agentes assumem, ou seja, se eles 
são empregados como executores ou como planejadores. Portanto, é possível que um único elemento efetue o planejamento de 
forma centralizada empregando diversos agentes ou que vários planejadores, de forma distribuída, calculem um plano para um 
único agente executor \cite{cooperation}.

Quando existe apenas um agente, os conjuntos de ações disponíveis e objetivos são decorrentes de uma única fonte, tornando 
o problema em um planejamento individualizado. Nesse contexto, algumas condições surgem naturalmente e caracterizam o planejamento de um 
único agente (\textit{Single-Agent Planning - SAP}) \cite{introMAP2005}. Em primeiro lugar, todas as capacidades de alterações do estado 
ambiente, decorrentes dos efeitos causados pelas execuções das ações, estão concentradas em um único agente e portanto não existe a 
possibilidade de nenhum tipo de divisão de responsabilidade. Entretanto, esse isolamento evita a ocorrência de questões relacionadas à 
privacidade das informações. Pelo mesmo motivo, não há necessidade de utilização de um protocolo de coordenação que gerencie a 
interação dos agentes. Por último, a presença de um único agente faz com que todos os objetivos definidos sejam 
ao mesmo tempo globais e individuais e portanto não há necessidade de utilização de um protocolo de alocação de tarefas. 
Considerando a definição mais simples, nesse contexto o único agente presente é tanto executor quanto planejador.

Por outro lado, quando o ambiente apresentar mais de uma entidade, o MAP pode ser melhor definido como o problema da busca do 
plano realizado e destinado a um grupo de agentes \cite{introMAP2009}. Enquanto no cenário 
SAP existe exclusivamente a necessidade de um componente de planejamento, nesse segundo caso há necessidade 
de gerenciar as interdependências entre as atividades, isto é, coordenar a realização dos esforços. A presença desses dois 
fatores permite aprimorar a definição de planejamento multiagente, conforme apresentado pela Definição \ref{def:map}, proveniente 
de \cite{introMAP2005}:

\begin{defn}
 Dada uma descrição do estado inicial, um conjunto de objetivos, um conjunto de (no mínimo dois) 
agentes, e para cada agente um conjunto de suas capacidades e seus objetivos individuais, encontre um plano para cada agente que  
satisfaça seus objetivos individuais, de tal forma que esses planos juntos são coordenados e garantam que os objetivos globais 
também sejam satisfeitos.
\label{def:map}
\end{defn}

A partir dessa definição, um problema de planejamento multiagente pode ser formalmente definido pela tupla $\Pi$ apresentada na 
Definição \ref{def:tuplaMAP} de \cite{fmap}:

\begin{defn}
  $\Pi = <Ag,V, O,I, G>$, 
  onde:
  \begin{itemize}
  \item $Ag=\{ag_1, ag_2, \ldots, ag_n\}$ é um conjunto finito de agentes;
  \item $V$ é o conjunto de variáveis de estado, cada qual com seu domínio $D(v)|v \in V$;
  \item $O$ é o conjunto de operadores, isto é, $O = \bigcup\limits_{i=1}^{n}O_{ag_i}$;
  \item $I$ é o estado inicial, isto é, $I = \bigcup\limits_{i=1}^{n}I_{ag_i}$;
  \item $G$ é o conjunto de objetivos, isto é, $G = \bigcup\limits_{i=1}^{n}G_{ag_i}$.
  \end{itemize}
  \label{def:tuplaMAP}
\end{defn}

As Definições \ref{def:map} e \ref{def:tuplaMAP} reforçam a importância de um protocolo de coordenação. Enriquecendo a análise 
com a taxonomia 
apresentada pela Figura \ref{img:taxonomia} é possível verificar que é através de um processo de planejamento que os agentes 
efetivamente trabalham de maneira cooperativa.

Portanto, os conceitos de cooperação e competição surgem naturalmente em um cenário de MAP. Nesse sentido, a necessidade de uma 
interação construtiva entre os agentes pode ser decorrente de alguns fatores. Para facilitar esse entendimento, dois casos 
de planejamento automatizado clássico serão apresentados e utilizados.

O caso \textit{rovers} consiste em um conjunto de robôs (agentes) que devem percorrer uma região com o objetivo de 
fotografá-la bem como de coletar amostras de rocha e solo. Nesse contexto, os agentes possuem os mesmos conjuntos 
de modelos de ações, porém os operadores instanciados podem ser diferentes devido à presença ou ausência de recursos, como 
equipamentos de imagem e coleta. Logo, o problema de planejamento $P$ pode ser diferente para cada agente (Definição 
\ref{def:problema}. A Figura \ref{img:rovers} ilustra o problema \textit{rovers}.

\begin{figure}[!ht]
 \centering
 \includegraphics[width=0.7\textwidth]{images/rovers.png}
 \label{img:rovers}
 \caption{Exemplo de problema de planejamento no domínio \textit{rovers}.}
\end{figure}

O segundo caso, conhecido como \textit{logistics}, retrata a situação de entrega de materiais entre cidades utilizando 
agentes de dois tipos. O tipo \textit{avião} é capaz de realizar a viagem entre cidades transportando os objetos que devem ser 
entregues. Por sua vez, agentes do tipo \textit{caminhão} são limitados ao deslocamento dentro de um única cidade. Nesse 
contexto, a 
questão de necessidade de cooperação entre os executores fica clara posto que o transporte de um material, inicialmente na 
Cidade A para um destino na Cidade B, deve envolver agentes dos dois tipos. O problema é ilustrado na Figura 
\ref{img:logistics}

\begin{figure}[!ht]
 \centering
 \includegraphics[width=0.99\textwidth]{images/logistics.png}
 \label{img:logistics}
 \caption{Exemplo de problema de planejamento do tipo \textit{logistics}.}
\end{figure}

Os fatores que implicam na necessidade de cooperação em um planejamento multiagente é estudado em 
\citet{cooperation}. Nesse trabalho, os agentes são classificados como homogêneos ou heterogêneos de acordo com suas variáveis de 
estado e suas capacidades. No primeiro caso, os agentes possuem conjuntos idênticos de operadores e portanto as variáveis de 
estado e capacidades também são iguais. Consequentemente, qualquer agente é capaz de calcular e executar um plano que satisfaz os 
objetivos. No caso de agentes heterogêneos, essa condição é resultante de diferenças entre os conjuntos de agentes e operadores. 
No exemplo 
\textit{logistics}, os agentes do tipo \textit{caminhão} e \textit{avião} possuem capacidades distintas, isto é, transportes 
dentro da mesma cidade e viagens entre cidades, respectivamente.

Como a pesquisa de \cite{cooperation} tem foco no papel de executores dos agentes, 
% segundo a definição do início do capítulo , 
a análise apresentada é fortemente baseada no conjunto de capacidades (operadores) de cada elemento do sistema. Entretanto, não é 
apenas nesse trabalho que o conceito de cooperação é observado sob esta dimensão. É comum verificar nos trabalhos correlatos que 
os problemas de planejamento são classificados conforme \cite{fmap}, da seguinte forma:

\begin{itemize}
 \item Fracamente acoplados (\textit{loosely-coupled}): os agentes possuem as mesmas capacidades de tal forma que cada agente 
pode individualmente satisfazer cada objetivo. As atividades podem ser executadas sem a necessidade de cooperação entre os 
agentes. O caso \textit{rovers} é classificado dessa maneira;
 \item Fortemente acoplados (\textit{tightly-coupled}): os agente precisam interagir entre si para que os objetivos possam ser 
satisfeitos. O caso \textit{logistics} é classificado dessa maneira.
\end{itemize}

Entretanto, como essa classificação baseia-se no conjunto de capacidades dos agentes, algumas condições para a 
execução do planejamento não são consideradas sob a ótica da cooperação e consequentemente da coordenação das atividades. Um 
exemplo seria um cenário semelhante àquele descrito na Figura \ref{img:rovers}, porém com os dois agentes possuindo capacidades 
diferentes ao mesmo tempo que não precisam interagir para satisfazer subconjuntos dos objetivos. Em outras 
palavras, sendo o \textit{Robô 1} capaz de coletar rochas enquanto o \textit{Robô 2} tem a capacidade de fotografar, nenhum deles 
depende do outro para executar tais tarefas. 
% Nessas condições, o caso \textit{rovers} não seria classificado como fracamente ou 
% fortemente acoplado. Existe, portanto, uma interseção que impede que os problemas de planejamento sejam tratados como conjuntos 
% disjuntos. 
Um problema nesse cenário seria atribuir objetivos aos agentes negligenciando essas capacidades.

A questão é que quando se deseja avaliar os problemas de planejamento considerando os aspectos referentes tanto ao planejamento 
quanto à execução do plano, a análise do conjunto de capacidades é necessária mas não é suficiente. Dessa forma, uma extensão da 
proposta apresentada em \citet{cooperation}, considerando não apenas a homogeneidade ou heterogeneidade dos agentes mas também
capacidade de cada um planejar suas atividades de forma individual, configura uma classificação mais abrangente dos problemas.
A Figura \ref{img:taxonomia_MAP} apresenta tal classificação.

\begin{figure}[htp]
 \centering
 \includegraphics[width=0.6\textwidth]{images/taxonomia_MAP.png}
 \caption{Classificação de MAP.}
 \label{img:taxonomia_MAP}
\end{figure}

Portanto, a cooperação deve ser buscada tanto na fase de planejamento quanto na de execução dos planos permitindo que:
\begin{itemize}
 \item agentes homogêneos (fracamente acoplados) possam planejar e executar suas ações de maneira independente, sem a necessidade 
de interação entre eles;
 \item agentes heterogêneos (fortemente acoplados) cooperem nas suas tarefas;
 \item agente com conjuntos de operadores diferentes porém capazes de satisfazer certos objetivos individualmente, coordenem 
quais 
metas cada um atenderá durante seus planejamentos e executem os planos de forma independente.
\end{itemize}

As diversas abordagens de MAP estão situadas dentro de um espaço tri-dimensional formado pelos eixos de dependência de recursos, 
cooperação e nível de comunicação \cite{introMAP2009}. Na dimensão que representa a relação entre os recursos, esses podem ser 
tanto independentes, quando não há qualquer compartilhamento ou dependência entre elas, quanto fortemente relacionados, no caso de 
ações conjuntas ou divisão de recursos. 
No eixo que mede a cooperação, os agentes são classificados em função da intenção de otimizar suas utilidades. Nesse sentido, os 
agentes podem ser cooperativos quando buscam atingir tanto os objetivos globais quanto os individuais.
% ou de interesse próprio quando preocupam-se apenas com seus estados finais desejáveis. 
Na terceira dimensão, o nível de comunicação é avaliado desde um cenário no qual nenhuma mensagem pode ser trocada até aquele que 
permite comunicações confiáveis. Essa definição é importante pois determina se a coordenação deverá ser feita antes ou durante o 
processo de solução do problema.

Em geral, as etapas para solução de um problema de MAP são compostas pelas seguintes etapas que, no entanto, não são obrigatórias 
em 
sua totalidade \cite{introMAP2005,introMAP2009,DOMAPS}: 

\begin{enumerate}
 
 \item Os objetivos globais são decompostos em conjuntos menores que podem ser feitos por um único agente; 
 \item Alocação dos objetivos aos agentes seguindo estratégias centralizadas ou distribuídas, o que influencia diretamente nos 
níveis de autonomia e privacidade. Esses métodos de alocação podem ainda considerar informações sobre as ações como, por exemplo, 
valores de retorno, custos de execução e até mesmo incertezas quando tratar-se de ambientes não-determinísticos;

\item Coordenação antes do planejamento, isto é, antes da computação dos planos. Nessa fase, as regras e 
condições de execução são definidas a fim de evitar conflitos durante o planejamento individual, isto é, situações nas quais uma 
ação pode desfazer um efeito já alcançado por outra ação;

\item As soluções são calculadas e cada agente pode utilizar um algoritmo de planejamento diferente;

\item Os planos individuais encontrados na fase anterior precisam ser integrados para formar um único plano conjunto ou para 
corrigir eventuais conflitos que podem surgir apesar do esforço da etapa de coordenação.
\end{enumerate}

% Em \cite{teamwork} as fases de reconhecimento de potencial, formação de equipe, planejamento e execução são descritas como 
% necessárias para atingir um estado de cooperação efetiva. 

Em \citet{teamwork}, as fases necessárias para atingir um estado de cooperação efetiva são definidas como:

\begin{enumerate}
 \item Reconhecimento de potencial - o trabalho em equipe começa quando o ambiente multiagente reconhece o potencial para a 
cooperação na busca pela satisfação dos objetivos. Nesse estágio, um conjunto de agentes é selecionado a partir dos recursos 
disponíveis e as entidades escolhidas devem ser colaborativas e também apresentar capacidades relevantes ao problema;

\item Formação de equipe: os agentes selecionados assumem o compromisso de atingir os objetivos 
de forma cooperativa e para tanto definem o protocolo de comunicação e interação que será seguido;

\item Planejamento: a etapa da geração do plano é iniciada e pode ser subdividida nas cinco fases descritas em 
\citet{introMAP2005,introMAP2009,DOMAPS}: decomposição, alocação, coordenação, planejamento e integração. A composição 
dessas fases depende das condições de execução e portanto nem sempre todas serão necessárias ou empregadas nessa etapa 
\cite{introMAP2005};

\item Execução: fase de execução que inclui tanto a realização das ações quanto um processo de reconfiguração. 

\end{enumerate}

O processo de reconfiguração da fase de execução é justificado para ambientes dinâmicos, os quais podem sofrer mudanças 
provocadas por eventos diferentes dos efeitos das ações. Esse processo deve permear todas as fases anteriores para fornecer estratégias que 
evitem que o dinamismo do ambiente inviabilize a execução de um plano já calculado. 

Dessa forma, pode existir a necessidade de ativação das 
fases já executadas, como por exemplo, a de seleção de potenciais entidades com o objetivo de buscar tanto um substituto para um agente 
impossibilitado de continuar quanto um novo elemento que possua capacidades requeridas pela mudança do ambiente. Essas alterações 
provavelmente provocarão a reexecução das fases seguintes, seja para ratificar ou retificar os protocolos de comunicação e interação 
escolhidos, seja para recalcular os planos.

No Capítulo \ref{cap:trabalhosCorrelatos} é apresentada uma revisão da literatura na qual são detalhados alguns trabalhos que 
configuram o estado da 
arte na área de MAP.


%TODO: além dessas fases, descrever as fases do teamwork

%TODO: inserir figuras