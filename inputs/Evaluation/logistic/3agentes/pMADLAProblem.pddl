(define
	(problem tmpProblem)
	(:domain tmpDomain)
	(:objects
		truck2 truck1 - truck
		obj13 obj12 obj22 obj21 obj11 obj23 - package
		apt2 apt1 pos2 pos1 - location
		cit2 cit1 - city
		plane1 - plane
	)
	(:init
		(atPackage obj11 pos1)
		(isAirport apt2)
		(inCity apt2 cit2)
		(atPackage obj12 pos1)
		(atPlane plane1 apt1)
		(inCity pos1 cit1)
		(atPackage obj21 pos2)
		(isAirport apt1)
		(inCity pos2 cit2)
		(atTruck truck1 pos1)
		(atTruck truck2 pos2)
		(inCity apt1 cit1)
		(atPackage obj13 pos1)
		(atPackage obj22 pos2)
		(atPackage obj23 pos2)
	)
	(:goal
		(and
			(atPackage obj23 pos1)
			(atPackage obj21 pos1)
			(atPackage obj11 apt1)
			(atPackage obj13 apt1)
		)
	)
)